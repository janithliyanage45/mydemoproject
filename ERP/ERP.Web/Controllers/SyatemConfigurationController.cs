﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;
using ERP.Core;
using ERP.Core.Entities;
using ERP.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using NHibernate.Linq;

namespace ERP.Web.Controllers
{
    public class SystemConfigurationController : Controller
    {

        public ActionResult Edit()
        {
            using (new SessionScope())
            {
                var fields = "";
                var sysConfig = SysConfig.LoadConfig();
                var list = new List<SysConfigViewModel>();

                var customProps = new List<PropertyInfo>();

                foreach (PropertyInfo p in customProps)
                {
                    if (!p.IsDefined(typeof(SettingAttribute), false))
                        continue;

                    var scvm = new SysConfigViewModel();
                    var param = SysParam.GetParam(SessionScope.CurrentSession, p.Name);
                    if (param == null)
                    {
                        param = new SysParam();
                        param.Name = p.Name;
                        param.ValueType = SysParam.ToSysParamType(p.PropertyType);

                        var defValAttr = p.GetCustomAttributes(typeof(DefaultValueAttribute), false).FirstOrDefault() as DefaultValueAttribute;
                        param.Value = SysConfig.ToStringValue(defValAttr.Value);
                        SessionScope.CurrentSession.Save(param);

                        scvm.Name = p.Name;
                        fields += p.Name + ",";
                        var attrs = p.GetCustomAttributes(true);

                        foreach (object attr in attrs)
                        {
                            var catAttr = attr as CategoryAttribute;
                            if (catAttr != null)
                            {
                                scvm.Category = catAttr.Category;
                            }

                            var dNameAttr = attr as DisplayNameAttribute;
                            if (dNameAttr != null)
                            {
                                scvm.DisplayName = dNameAttr.DisplayName;
                            }
                        }
                        var val = defValAttr.Value;
                        scvm.Value = SysConfig.ToStringValue(val);
                        scvm.ValueType = SysParam.ToSysParamType(p.PropertyType);
                        if (scvm.ValueType == SystemParameterType.DATE)
                            scvm.Value = ((DateTime)val).ToString("dd/MM/yyyy");

                        list.Add(scvm);
                    }
                    else
                    {
                        scvm.Name = p.Name;
                        fields += p.Name + ",";
                        var attrs = p.GetCustomAttributes(true);

                        foreach (object attr in attrs)
                        {
                            var catAttr = attr as CategoryAttribute;
                            if (catAttr != null)
                            {
                                scvm.Category = catAttr.Category;
                            }

                            var dNameAttr = attr as DisplayNameAttribute;
                            if (dNameAttr != null)
                            {
                                scvm.DisplayName = dNameAttr.DisplayName;
                            }
                        }
                        var val = param.ObjectValue;
                        scvm.Value = SysConfig.ToStringValue(val);
                        scvm.ValueType = SysParam.ToSysParamType(p.PropertyType);
                        if (scvm.ValueType == SystemParameterType.DATE)
                            scvm.Value = ((DateTime)val).ToString("dd/MM/yyyy");

                        list.Add(scvm);

                    }
                }


                var props = typeof(SysConfig).GetProperties();
                foreach (PropertyInfo p in props)
                {
                    if (!p.IsDefined(typeof(SettingAttribute), false))
                        continue;

                    if (!Context.Context.UserContext.UserInfo.IsEvisionUser)
                    {
                        if (p.IsDefined(typeof(IsEvision), false))
                            continue;
                    }


                    var scvm = new SysConfigViewModel();
                    scvm.Name = p.Name;
                    fields += p.Name + ",";
                    var attrs = p.GetCustomAttributes(true);

                    foreach (object attr in attrs)
                    {
                        var catAttr = attr as CategoryAttribute;
                        if (catAttr != null)
                        {
                            scvm.Category = catAttr.Category;
                        }

                        var dNameAttr = attr as DisplayNameAttribute;
                        if (dNameAttr != null)
                        {
                            scvm.DisplayName = dNameAttr.DisplayName;
                        }
                    }
                    var val = p.GetValue(sysConfig, null);
                    scvm.Value = SysConfig.ToStringValue(val);
                    scvm.ValueType = SysParam.ToSysParamType(p.PropertyType);
                    if (scvm.ValueType == SystemParameterType.DATE)
                        scvm.Value = ((DateTime)val).ToString("dd/MM/yyyy");
                    list.Add(scvm);
                }

                var dict = new Dictionary<string, List<SysConfigViewModel>>();
                foreach (var scvm in list)
                {
                    if (!dict.ContainsKey(scvm.Category))
                    {
                        var temp = new List<SysConfigViewModel>();
                        temp.Add(scvm);
                        dict.Add(scvm.Category, temp);
                    }
                    else
                    {
                        var temp = dict[scvm.Category];
                        temp.Add(scvm);
                    }
                }

                ;
                var vm = new SysConfigListViewModel();
                vm.Value = dict;
                fields = fields.Remove(fields.Length - 1, 1);
                vm.FieldNames = fields;
                return View(vm);
            }
        }

        [HttpPost]
        public ActionResult Save(string values)
        {
            try
            {

                var customProps = new List<PropertyInfo>();

                var dict = new Dictionary<string, string>();
                using (new SessionScope())
                using (var txn = SessionScope.CurrentSession.BeginTransaction())
                {
                    var fieldValues = values.Split('~');
                    foreach (var fieldValue in fieldValues)
                    {
                        var data = fieldValue.Split('`');
                        dict.Add(data[0], data[1]);
                    }

                    var props = typeof(SysConfig).GetProperties().ToList();
                    customProps.AddRange(props);

                    foreach (var p in customProps)
                    {
                        if (!p.IsDefined(typeof(SettingAttribute), false))
                            continue;

                        if (!Context.Context.UserContext.UserInfo.IsEvisionUser)
                        {
                            if (p.IsDefined(typeof(IsEvision), false))
                                continue;
                        }

                        var param = SysParam.GetParam(SessionScope.CurrentSession, p.Name);
                        var value = dict[param.Name];
                        param.Value = value;
                        SessionScope.CurrentSession.Update(param);
                    }
                    txn.Commit();
                    return Json(new { Success = true });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = Utils.GetExceptionText(ex) });
            }
        }


    }
}
