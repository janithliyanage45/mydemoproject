﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace ERP.Web.ViewModels
{
    public class CustomerListViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Outstanding { get; set; }
        public string DOB { get; set; }
        public bool Active { get; set; }

        public override void InitMappings(Profile profile)
        {
        }
    }
}
