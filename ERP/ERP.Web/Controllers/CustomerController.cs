﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Core.Entities;
using ERP.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Controllers
{
    public class CustomerController : Controller
    {
        public IActionResult Index()
        {
            var customersView = new List<CustomerListViewModel>();
            using (new SessionScope())
            {
                var customers = SessionScope.CurrentSession.Query<Customer>().ToList();
                foreach (var customer in customers)
                {
                    var customerVm = new CustomerListViewModel
                    {
                        Id = customer.Id,
                        Code = customer.Code,
                        Name = customer.Name,
                        Outstanding = customer.Outstanding,
                        DOB = customer.DOB.ToString("yyyy-MM-dd"),
                        Active = customer.Active
                    };
                    customersView.Add(customerVm);
                }
            }
            return View(customersView);
        }

        public IActionResult Edit(int id = 0)
        {
            var cvm = new CustomerViewModel();
            using (new SessionScope())
            {
                var customer = id == 0 ? new Customer { Code = "NEW", Name = "NEW", DOB = System.DateTime.Today } : SessionScope.CurrentSession.Get<Customer>(id);
                cvm.Id = customer.Id;
                cvm.Code = customer.Code;
                cvm.Name = customer.Name;
                cvm.Active = customer.Active;
                cvm.DOB = customer.DOB;
                cvm.Outstanding = customer.Outstanding;
            }
            return View(cvm);
        }
        [HttpPost]
        public IActionResult Edit(CustomerViewModel cvm)
        {
            if (ModelState.IsValid)
            {
                using (new SessionScope())
                using (var txn = SessionScope.CurrentSession.BeginTransaction())
                {
                    var customer = cvm.Id == 0 ? new Customer { } : SessionScope.CurrentSession.Get<Customer>(cvm.Id);
                    customer.Code = cvm.Code;
                    customer.Name = cvm.Name;
                    customer.Active = cvm.Active;
                    customer.Outstanding = cvm.Outstanding;
                    customer.DOB = cvm.DOB ?? DateTime.Now;

                    SessionScope.CurrentSession.SaveOrUpdate(customer);
                    txn.Commit();
                }
                return RedirectToAction("Index");
            }
            return View(cvm);
        }

        public IActionResult Delete(int id)
        {
            using (new SessionScope())
            using (var txn = SessionScope.CurrentSession.BeginTransaction())
            {
                var customer = SessionScope.CurrentSession.Get<Customer>(id);
                SessionScope.CurrentSession.Delete(customer);
                txn.Commit();
            }
            return RedirectToAction("Index");
        }
    }
}