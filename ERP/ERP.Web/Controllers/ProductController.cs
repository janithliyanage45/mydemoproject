﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Core.Entities;
using ERP.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            var productVms = new List<ProductListViewModel>();
            using (new SessionScope())
            {
                var products = SessionScope.CurrentSession.Query<Product>().ToList();
                foreach (var product in products)
                {
                    var proudutVm = new ProductListViewModel
                    {
                        Id = product.Id,
                        Active = product.Active,
                        Code = product.Code,
                        Name = product.Name,
                    };

                    productVms.Add(proudutVm);
                }
            }
            return View(productVms);
        }

        public ActionResult Edit(int id = 0)
        {
            var vm = new ProductViewModel();

            using (new SessionScope())
            {
                var produt = id == 0 ? new Product { Code = "NEW", Name = "NEW", CreatedDate = System.DateTime.Today } : SessionScope.CurrentSession.Get<Product>(id);
                vm.Id = produt.Id;
                vm.Code = produt.Code;
                vm.Name = produt.Name;
                vm.Price = produt.Price;
                vm.Active = produt.Active;
                vm.CreatedDate = produt.CreatedDate;
            }
            return View(vm);
        }
        [HttpPost]
        public ActionResult Edit(ProductViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (new SessionScope())
                    using (var txn = SessionScope.CurrentSession.BeginTransaction())
                    {
                        var produt = vm.Id == 0 ? new Product() : SessionScope.CurrentSession.Get<Product>(vm.Id);
                        produt.Code = vm.Code;
                        produt.Name = vm.Name;
                        produt.Active = vm.Active;
                        produt.Price = vm.Price;
                        produt.CreatedDate = vm.CreatedDate ?? DateTime.Now;
                        SessionScope.CurrentSession.SaveOrUpdate(produt);
                        txn.Commit();
                    }

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    return View(vm);
                }
            }
            return View(vm);
        }

        public IActionResult Delete(int id)
        {
            using (new SessionScope())
            using (var txn = SessionScope.CurrentSession.BeginTransaction())
            {
                var product = SessionScope.CurrentSession.Get<Product>(id);
                SessionScope.CurrentSession.Delete(product);
                txn.Commit();
            }
            return RedirectToAction("Index");
        }
    }
}