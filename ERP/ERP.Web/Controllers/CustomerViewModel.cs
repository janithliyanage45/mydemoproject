﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace ERP.Web.ViewModels
{
    public class CustomerViewModel : ViewModelBase
    {
        [Hidden]
        public int Id { get; set; }
        [Required]
        [DisplayName("Customer Code")]
        public string Code { get; set; }
        [Required]
        [DisplayName("Customer Name")]
        public string Name { get; set; }
        public decimal Outstanding { get; set; }
        public DateTime? DOB { get; set; }
        public bool Active { get; set; } 

        public override void InitMappings(Profile profile)
        {
        }
    }
}
