﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ERP.Core;
using ERP.Core.Entities;
using ERP.Web.Context;
using ERP.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.CompilerServices;
using NHibernate.Linq;
using WebApplication2.Models;
using Microsoft.Extensions.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace ERP.Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        private IConfiguration _config;

        public LoginController()
        {
            // _config = config;
        }

        public ActionResult Index()
        {
            var vm = new LoginViewModel();
            vm.Init();
            Context.Context.TerminateSession();
            return View(vm);
        }

        public ActionResult Init()
        {
            NHibernateManager.CreateDatabase();
            return Content("DB created successfully!");
        }

        public ActionResult Validate(LoginViewModel vm)
        {
            using (new SessionScope())
            {
                vm.Init();

                var user = SessionScope.CurrentSession.Query<User>()
                    .Where(x => x.Username == vm.UserName).Fetch(x => x.Role).ToFuture().ToList().FirstOrDefault();


                if (user == null)
                {
                    @ViewBag.ErrorMsg = "Invalid username";
                    Core.Utils.WriteAuditTrail(0, vm.UserName, "LOGIN", "LOGIN_FAIL", "Invalid username");
                    return View("Index", vm);
                }

                if (user.LoginWithAD)
                {
                    @ViewBag.ErrorMsg = "Sign in not allowed. Please sign in with Azure active directory";
                    Core.Utils.WriteAuditTrail(0, vm.UserName, "LOGIN", "LOGIN_FAIL", "Sign in not allowed. (AD Only User)");
                    return View("Index", vm);
                }

                if (user.Locked)
                {
                    if (user.LockedDate.AddMinutes(15) < DateTime.Now)
                    {
                        user.Locked = false;
                        user.FailedLoginAttempts = 0;
                        DAO.Save(user);
                    }
                    else
                    {
                        @ViewBag.ErrorMsg = "User locked!, Please try again in " + (15 - (DateTime.Now - user.LockedDate).Minutes) + " minutes!";
                        Core.Utils.WriteAuditTrail(user.Id, vm.UserName, "LOGIN", "LOGIN_FAIL", "User locked!");
                        return View("Index", vm);
                    }
                }

                if (!user.Active)
                {
                    @ViewBag.ErrorMsg = "User inactive!";
                    Core.Utils.WriteAuditTrail(user.Id, vm.UserName, "LOGIN", "LOGIN_FAIL", "User inactive!");
                    return View("Index", vm);
                }


                var encodedPw = Core.Utils.GetEncodedPassword(vm.Password);
                if (user.Password != encodedPw)
                {
                    @ViewBag.ErrorMsg = "Invalid password";

                    ++user.FailedLoginAttempts;
                    if (user.FailedLoginAttempts >= 5)
                    {
                        user.Locked = true;
                        user.LockedDate = DateTime.Now;
                        @ViewBag.ErrorMsg = "User locked due to invalid password! Please try again in 15 minutes!";
                    }

                    Core.Utils.WriteAuditTrail(user.Id, vm.UserName, "LOGIN", "LOGIN_FAIL", @ViewBag.ErrorMsg);
                    DAO.Save(user);

                    return View("Index", vm);
                }

                if (user.FailedLoginAttempts > 0)
                {
                    user.FailedLoginAttempts = 0;
                    DAO<User>.Save(user);
                }

                Core.Utils.WriteAuditTrail(user.Id, vm.UserName, "LOGIN", "LOGIN_SUCCESS");
                // HttpContext.Session.SetObject("CurrentUser", user);
                var path = Path.Combine(Globals.HostingEnvironment.ContentRootPath, "DynamicReports");

                Context.Context.InitiateSession(user, path);
                if (user.Type != UserType.External)
                {
                    return RedirectToAction("Index", "Dashboard1");
                }
                else
                {
                    return RedirectToAction("ExternalIndex", "Dashboard1");
                }

            }
        }

    }
}

