﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ERP.Core.Entities;
using ERP.Web.ViewModels;
using ERP.Core;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace ERP.Web.Controllers
{
    public class UserRoleController : BaseAuthController
    {
        public IActionResult Index()
        {
            var vm = DAO<UserRole>.ListAll().Select(Globals.Mapper.Map<UserRole, UserRoleListViewModel>).ToList();
            return View(vm);
        }

        public IActionResult Edit(int id = 0)
        {
            using (new SessionScope())
            {
                var role = id == 0
                    ? new UserRole()
                    {
                        Privileges = new HashSet<RolePrivilege>(),
                    }
                    : DAO<UserRole>.Get(id);

                var vm = new UserRoleViewModel() { Id = role.Id, Name = role.Name, IsSuperUser = role.IsSuperUser };
                var privs = role.GetPrivileges();
                vm.Init(privs);

                return View(vm);
            }
        }

        [HttpPost]
        public IActionResult Save(string data, string reportPrevileges, string dynamicReportPrevileges)
        {
            try
            {
                using (new SessionScope())
                {
                    var dto = JsonConvert.DeserializeObject<UserRoleSaveDTO>(data);
                    var role = dto.Id == 0
                        ? new UserRole() { Privileges = new HashSet<RolePrivilege>() }
                        : DAO<UserRole>.Get(dto.Id);
                    role.Name = dto.Name;
                    role.IsSuperUser = dto.IsSuperUser;

                    var privs = role.GetPrivileges();
                    privs.ResetAllPrivileges(false);
                    foreach (var privCode in dto.GrantedPrivileges)
                        privs.SetPrivilege(privCode, true);

                    role.ApplyPrivileges(privs);

                    if (string.IsNullOrEmpty(role.Name))
                        throw new Exception("Please enter user role name!");

                    DAO<UserRole>.Save(role);
                    return Json(new { Success = true });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = Utils.GetExceptionText(ex) });
            }

        }
    }

    public class UserRoleSaveDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSuperUser { get; set; }
        public string[] GrantedPrivileges { get; set; }
    }
}