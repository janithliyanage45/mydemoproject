﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ERP.Core.Entities;
using ERP.Web.ViewModels;
using ERP.Core;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.Web.Controllers
{
    public class UserController : BaseAuthController
    {

        public IActionResult Index()
        {
            if (TempData["ErrorMsg"] != null)
                ViewBag.ErrorMsg = TempData["ErrorMsg"];

            using (new SessionScope())
            {

                if (Context.Context.UserContext == null)
                {
                    return RedirectToAction("Index", "Login");
                }

                //if (Context.Context.SycConfigs.IsSFA && !Context.Context.UserContext.Branch.IsHeadOffice)
                //{
                //    var users = SessionScope.CurrentSession.Query<SalesHierarchyNode>().Where(x => x.Distributor == Context.Context.UserContext.Branch).ToList().Select(x=>x.User);

                //    var vm = users.Select(Globals.Mapper.Map<User, UserListViewModel>)
                //            .ToList();
                //    return View(vm);
                //}
                //else
                //{
                var vm = DAO<User>.ListAll().Select(Globals.Mapper.Map<User, UserListViewModel>).ToList();
                return View(vm);
                //}
            }
        }

        public IActionResult Edit(int id = 0)
        {
            using (new SessionScope())
            {
                var user = id == 0 ? new User() { Code = "AUTO GENERATE", Username = "NEW", Active = true } : DAO<User>.Get(id);
                var vm = Globals.Mapper.Map<User, UserViewModel>(user);
                vm.TypeId = (int)user.Type;
                vm.Init();
                return View(vm);
            }
        }

        [HttpPost]
        public IActionResult Edit(UserViewModel vm)
        {
            using (new SessionScope())
            using (var txn = SessionScope.CurrentSession.BeginTransaction())
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (!ModelState.IsValid)
                {


                    var taks = new List<BasicEntity>();
                    var initialSelectionTask = new int[] { };
                    vm.TaskIdList = new MultiSelectList(taks, "Id", "Name", initialSelectionTask);


                    vm.Init();
                    return View(vm);
                }

                try
                {
                    var user = vm.Id == 0
                        ? new User
                        {
                        }
                        : DAO<User>.Get(vm.Id);

                    var oldUser = new User()
                    {

                    };

                    //if (vm.Id != 0)
                    //{
                    //    var oldvm = Globals.Mapper.Map<User, UserViewModel>(user);
                    //    Globals.Mapper.Map(oldvm, oldUser);

                    //    var existingNodeUser = SessionScope.CurrentSession.Query<User>().FirstOrDefault(x => x.SalesHierarchyNode != null && x.SalesHierarchyNode.Id == vm.SalesHierarchyNodeId && x.Id != vm.Id);
                    //    if (existingNodeUser != null)
                    //        throw new Exception("Sales hierachy node already assigned");
                    //}
                    //else
                    //{
                    //    var existingNodeUser = SessionScope.CurrentSession.Query<User>().FirstOrDefault(x => x.SalesHierarchyNode != null && x.SalesHierarchyNode.Id == vm.SalesHierarchyNodeId);
                    //    if (existingNodeUser != null)
                    //        throw new Exception("Sales hierachy node already assigned");
                    //}


                    if (!string.IsNullOrEmpty(vm.PdaImei))
                    {
                        if (vm.Id == 0)
                        {
                            var existingUser =
                                SessionScope.CurrentSession.Query<User>().FirstOrDefault(x => x.PdaImei == vm.PdaImei);
                            if (existingUser != null)
                            {
                                throw new Exception("Duplicate IMEI Number");
                            }
                        }
                        else
                        {
                            var existingUser =
                                SessionScope.CurrentSession.Query<User>().FirstOrDefault(x => x.PdaImei == vm.PdaImei && x.Id != vm.Id);
                            if (existingUser != null)
                            {
                                throw new Exception("Duplicate IMEI Number");
                            }
                        }
                    }

                    if (vm.Id == 0)
                    {


                        //if (!vm.EnableSFA && string.IsNullOrEmpty(vm.Code))
                        //{
                        //    vm.Code = vm.Username;
                        //}

                        var existingUser =
                           SessionScope.CurrentSession.Query<User>().FirstOrDefault(x => x.Code == vm.Code);

                        if (existingUser != null)
                        {
                            throw new Exception("Duplicate Code");
                        }
                    }
                    else
                    {
                        if (!vm.EnableSFA && string.IsNullOrEmpty(vm.Code))
                        {
                            vm.Code = vm.Username;
                        }

                        var existingUser =
                            SessionScope.CurrentSession.Query<User>().FirstOrDefault(x => x.Code == vm.Code && x.Id != vm.Id);

                        if (existingUser != null)
                        {
                            throw new Exception("Duplicate Code");
                        }
                    }

                    if (vm.RestrictLoginTimes && vm.LoginTimePolicyId == null)
                        throw new Exception("Please select Login Time Policy!");

                    if (vm.CreditApprovalRequest && string.IsNullOrEmpty(vm.MobileNo))
                        throw new Exception("Please Add Mobile No!");

                    Globals.Mapper.Map(vm, user);

                    user.Role = DAO<UserRole>.Get(vm.RoleId);

                    //user.AllowedBranches = "";
                    //if (vm.AllowedBranchId != null)
                    //    foreach (var branchId in vm.AllowedBranchId)
                    //        user.AllowedBranches += branchId + ",";
                    //user.AllowedBranches = user.AllowedBranches.TrimEnd(',');

                    if (vm.SetPassword)
                    {
                        if (string.IsNullOrEmpty(vm.NewPassword))
                            throw new Exception("New password is empty!");

                        user.Password = Utils.GetEncodedPassword(vm.NewPassword);
                        user.NewPassword = vm.NewPassword;
                    }
                    user.Supervisor = vm.SupervisorId != null ? DAO<User>.Get(vm.SupervisorId) : null;





                    //user.SalesHierarchyNode = vm.SalesHierarchyNodeId != null ? DAO<SalesHierarchyNode>.Get(vm.SalesHierarchyNodeId) : null;

                    SessionScope.CurrentSession.SaveOrUpdate(user);
                    txn.Commit();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, Utils.GetExceptionText(ex));

                    vm.Init();
                    return View(vm);
                }

                return RedirectToAction("Index");
            }

        }

        public IActionResult ChangePassword(int id = 0)
        {
            var model = new PasswordViewModel();
            model.Id = Context.Context.UserContext.UserInfo.Id;
            model.IsError = true;
            return View(model);
        }

        //private static void CreateSerialPrefix(User user)
        //{
        //    if (string.IsNullOrEmpty(user.Code))
        //        throw new Exception("Please Add Code!");
        //    var all = user.AllowedBranches.Split(',');

        //    if (all.Length != 1)
        //        throw new Exception("Please Add Single Branch!");

        //    if (all[0] == "0")
        //        throw new Exception("Please Add Single Branch!");

        //    if (string.IsNullOrEmpty(user.SalesSerialPrefix))
        //    {
        //        user.SalesSerialPrefix = user.Code + "SI";
        //    }
        //    if (string.IsNullOrEmpty(user.ReturnSerialPrefix))
        //    {
        //        user.ReturnSerialPrefix = user.Code + "RI";
        //    }
        //    if (string.IsNullOrEmpty(user.LastPaymentSerialPrefix))
        //    {
        //        user.LastPaymentSerialPrefix = user.Code + "RE";
        //    }
        //    if (string.IsNullOrEmpty(user.SaleOrderSerialPrefix))
        //    {
        //        user.SaleOrderSerialPrefix = user.Code + "SO";
        //    }
        //    if (string.IsNullOrEmpty(user.ConsumptionJournalSerialPrefix))
        //    {
        //        user.ConsumptionJournalSerialPrefix = user.Code + "CJ";
        //    }
        //    if (string.IsNullOrEmpty(user.ImageSerialPrefix))
        //    {
        //        user.ImageSerialPrefix = user.Code + "IMG";
        //    }
        //    if (string.IsNullOrEmpty(user.ReturnRequestSerialPrefix))
        //    {
        //        user.ReturnRequestSerialPrefix = user.Code + "RR";
        //    }
        //    if (string.IsNullOrEmpty(user.CallLostSerialPrefix))
        //    {
        //        user.CallLostSerialPrefix = user.Code + "CL";
        //    }
        //    if (string.IsNullOrEmpty(user.ServaySerialPrefix))
        //    {
        //        user.ServaySerialPrefix = user.Code + "SEY";
        //    }
        //    if (string.IsNullOrEmpty(user.CompetitorStockTakingSerialPrefix))
        //    {
        //        user.CompetitorStockTakingSerialPrefix = user.Code + "CSTSEY";
        //    }
        //    if (string.IsNullOrEmpty(user.StockTakingSerialPrefix))
        //    {
        //        user.StockTakingSerialPrefix = user.Code + "STSEY";
        //    }
        //    if (string.IsNullOrEmpty(user.CommonSerialPrefix))
        //    {
        //        user.CommonSerialPrefix = user.Code + "COMMON";
        //    }
        //    if (string.IsNullOrEmpty(user.LoadingRequestSerialPrefix))
        //    {
        //        user.LoadingRequestSerialPrefix = user.Code + "LR";
        //    }
        //    if (string.IsNullOrEmpty(user.LoanReturnSerialPrefix))
        //    {
        //        user.LoanReturnSerialPrefix = user.Code + "LNRTN";
        //    }
        //}

        [HttpPost]
        public IActionResult ChangePassword(PasswordViewModel model)
        {
            model.Message = null;
            model.Class = null;

            if (ModelState.IsValid)
            {
                try
                {
                    var currentPassword = Context.Context.UserContext.UserInfo.Password;
                    if (currentPassword != Utils.GetEncodedPassword(model.CurruntPassword))
                    {
                        model.IsError = true;
                        model.Message = "Current password doesn't match";
                        model.Class = "alert-danger";
                        return View(model);
                    }

                    var user = DAO<User>.Get(model.Id);
                    user.Password = Utils.GetEncodedPassword(model.NewPassword);
                    user.NewPassword = model.NewPassword;

                    DAO<User>.Save(user);
                    model.IsError = false;
                    model.Message = "Password successfuly changed";
                    model.Class = "alert-success";
                    return View(model);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, Utils.GetExceptionText(ex));
                    model.IsError = true;
                    model.Message = Utils.GetExceptionText(ex);
                    model.Class = "alert-danger";
                    return View(model);
                }
            }

            return View(model);
        }

        public IActionResult Delete(int id)
        {
            try
            {

                using (new DefaultSessionScope())
                {
                    var user = DAO<User>.Get(id);
                    DAO<User>.Delete(user);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["ErrorMsg"] = "Failed to delete! \nError: " + Utils.GetExceptionText(ex);
                return RedirectToAction("Index");
            }
        }

    }
}