﻿using System;
using ERP.Web.Filters.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Web.Controllers
{
    [BaseAuthenticationFilter]
    public class BaseAuthController : Controller
    {
    }

    public class PrivilegeAttribute : AuthorizeAttribute
    {
        public string Code { get; set; }
        public PrivilegeAttribute(string code)
        {
            Code = code;
        }

        //protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        //{
        //    return Context.Context.UserContext.HasPrivilege(Code);
        //}
    }
}
