﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using ERP.Core;
using ERP.Core.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json;
using NHibernate.Linq;
using Remotion.Linq.EagerFetching.Parsing;
using WebApplication2.Models;

namespace ERP.Web.Context
{
    public class Context
    {

        public static ConcurrentDictionary<Int32, UserContext> _userContextDict = new ConcurrentDictionary<Int32, UserContext>();
        public static ConcurrentDictionary<Int32, User> _userDict = new ConcurrentDictionary<Int32, User>();

        public static UserContext UserContext
        {
            get
            {
                var user = ErpHttpContext.Current.Session.GetObject<UserContext>("UserContext");
                return user;
            }
        }

        public static SysConfig SycConfigs
        {
            get
            {
                var configs = ErpHttpContext.Current.Session.GetObject<SysConfig>("SysConfigs");
                return configs;
            }
        }


        public static void InitiateSession(User user, string dynamicReportPath)
        {

            var userContext = new UserContext
            {
                UserInfo = user,
                SecurityPrivileges = user.Role.GetPrivileges(),
                RolePrivileges = SetPrivileges(user)
            };

            if (user.Role.IsSuperUser)
                userContext.SecurityPrivileges.ResetAllPrivileges(true);

            userContext.SetParameter("UserId", user.Id);
            userContext.SetParameter("UserName", user.Name);

            ErpHttpContext.Current.Session.SetObject("UserContext", userContext.UserInfo.Id);
            _userContextDict.TryAdd(userContext.UserInfo.Id, userContext);

            ErpHttpContext.Current.Session.SetObject("SysConfigs", userContext.UserInfo.Id);
        }

        public static void TerminateSession()
        {
            if (UserContext != null)
            {
                UserContext u = null;
                _userContextDict.TryRemove(UserContext.UserInfo.Id, out u);
            }
            ErpHttpContext.Current.Session.SetObject("UserContext", null);
        }

        private static Dictionary<string, bool> SetPrivileges(User user)
        {
            var privileges = user.Role.Privileges.ToDictionary(privilege => privilege.Code, privilege => privilege.Value);

            return privileges;
        }

        public static string GetPrivilegeCode(string propertyName)
        {
            var values = Regex.Split(propertyName, @"(?<!^)(?=[A-Z])");
            return string.Format("{0}:{1}", values[2].ToUpper(), values[1].ToUpper());
        }

        public static string GetIdText(List<int> idList)
        {
            var idText = "0";

            foreach (var id in idList)
            {
                idText += "," + id;
            }

            return idText;
        }
    }

    public static class ErpHttpContext
    {
        static IServiceProvider services = null;

        /// <summary>
        /// Provides static access to the framework's services provider
        /// </summary>
        public static IServiceProvider Services
        {
            get { return services; }
            set
            {
                if (services != null)
                {
                    throw new Exception("Can't set once a value has already been set.");
                }
                services = value;
            }
        }

        /// <summary>
        /// Provides static access to the current HttpContext
        /// </summary>
        public static HttpContext Current
        {
            get
            {
                IHttpContextAccessor httpContextAccessor = services.GetService(typeof(IHttpContextAccessor)) as IHttpContextAccessor;
                return httpContextAccessor?.HttpContext;
            }
        }

    }
}