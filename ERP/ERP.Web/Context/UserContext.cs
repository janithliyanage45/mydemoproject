﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ERP.Core.Entities;


namespace ERP.Web.Context
{
    

    public class UserContext
    {
        public UserContext()
        {
            AllowedCompanyIds = new List<int>();
        }

        public User UserInfo { get; set; }
        public SecurityPrivileges SecurityPrivileges { get; set; }
        public Dictionary<string, bool> RolePrivileges { get; set; }
        public Dictionary<string, bool> CustomPrivileges { get; set; }
        public bool IsAllBrachesAllowed { get; set; }


        public List<int> AllowedTerritoryIds { get; set; }

        public List<int> AllowedRepIds { get; set; }

        public List<int> AllowedDistributorIds { get; set; }

        public List<int> AllowedCompanyIds { get; set; }

        /*public static CP CusProvider { get; private set; }*/

        private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

        public void SetParameter(string key, object value)
        {
            _parameters[key] = value;
        }

        public object GetParameter(string key)
        {
            return _parameters[key];
        }

        public IDictionary<string, object> Parameters { get { return _parameters; } }

        public bool HasPrivilege(string privCode)
        {
            if (RolePrivileges.ContainsKey(privCode))
                return RolePrivileges[privCode];

            return false;
        }

    }
}
