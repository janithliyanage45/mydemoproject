﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.Web.Filters
{
    public class RequestDurationMiddleware
    {
        private readonly RequestDelegate _next;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public RequestDurationMiddleware(RequestDelegate next)
        {
            _next = next;
            //_logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var watch = Stopwatch.StartNew();
            await _next.Invoke(context);
            watch.Stop();

            _logger.Trace("DURATION: {0} - {1}", context.Request.Path, watch.ElapsedMilliseconds);
            var str = string.Format("DURATION: {0} - {1}", context.Request.Path, watch.ElapsedMilliseconds);
            Console.WriteLine(str);
        }
    }

    public static class RequestDurationMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestDurationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestDurationMiddleware>();
        }
    }
}
