﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using NLog;
using System;
using ERP.Core.Entities;
using ERP.Web.Filters.Authentication.Microsoft.AspNetCore.Mvc;
using ERP.Web.Context;
using WebApplication2.Models;
using Microsoft.AspNetCore.Http.Headers;

namespace ERP.Web.Filters.Authentication
{

    public class BaseAuthenticationFilter : ActionFilterAttribute, IAuthorizationFilter
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public void OnAuthorization(AuthorizationFilterContext filterContext)
        {
            var routeData = filterContext.RouteData;

            var action = routeData.GetRequiredString("action");
            var controller = routeData.GetRequiredString("controller");



            var method = filterContext.HttpContext.Request.Method;
            var ipAddress = filterContext.HttpContext.Request.Host;

            _logger.Trace("{0}: {1}/{2} from {3}", method, controller, action, ipAddress);

            if (controller == "StockTaking")
                return;

            if (controller == "User" && action == "ChangePassword")
                return;

            if (controller == "Invoice" && action == "GetNextPrintRequest" || controller == "PosSync")
                return;

            if (controller == "Product" && (action == "GetListForBarcodes" || action == "Search"))
                return;

            if (controller == "NewInvoice" && action == "GetInvoiceLineProdcutSerialForBarcodes")
                return;

            if (controller == "NewInvoice" && action == "DeleteInvoiceByPDA")
                return;

            if (controller == "NewInvoice" && action == "DownloadFile")
                return;

            if (controller == "AchievementDashboard" && (action == "GlobalIndex" || action == "GlobalVolumeWiseAchivement"))
                return;

            if (controller == "RepSummaryDashboard" && (action == "GlobalIndex"))
                return;

            if (controller == "ApprovalRequest" && (action == "AssetAgreementAssignmentSearchIndex" || action == "AssetTransferHistorySearchIndex" || action == "Create" || action == "Approved" || action == "Rejected" || action == "ActionURL" || action == "ActionURLIndex" || action == "BulkApprovalOrReject"))
                return;

            if (controller == "RouteManagerPDA")
                return;

            if (controller == "DynamicReporting" && action == "PDADrillDown")
                return;

            if (controller == "DynamicReporting" && (action == "Index" || action == "Criteria" || action == "Preview"))
                return;

            if (controller == "Rectifire" && action == "InitDB")
                return;

            if (controller == "Rectifire" && action == "CleanDB")
                return;

            if (controller == "Rectifire" && action == "CheckDB")
                return;

            if (controller == "Rectifire" && action == "CreateDB")
                return;

            if (controller == "NewInvoice" && action == "SetStatus")
                return;

            if (controller == "Report")
            {
                if (ErpHttpContext.Current.Session.GetObject<UserContext>("UserContext") == null)
                {
                    filterContext.Result = new RedirectResult("~/Login");
                    return;
                }

                return;
            }

            if (ErpHttpContext.Current.Session.GetObject<UserContext>("UserContext") == null)
            {
                filterContext.Result = new RedirectResult("~/Login");
                return;
            }

            if (Context.Context.UserContext.UserInfo.Role.IsSuperUser)
                return;

            if (action.StartsWith("Get") || action.StartsWith("GET")) //Ajax calls
                return;

            if ((controller == "DashBoard1" || controller == "Dashboard1") && (action == "AuthenticationError" || action == "Index" || action == "SwitchBranch"))
                return;

            //Validate Posting And Undo Posting
            if (controller == "Invoice")
            {
                if (action == "PostInvoiceTransaction" || action == "PostPaymentTransaction" || action == "PostTransaction")
                {
                    if (Context.Context.UserContext.HasPrivilege("InvoicePostTransaction"))
                        return;
                }

                if (action == "UndoInvoicePostTransaction" || action == "UndoPaymentPostTransaction" || action == "UndoPostTransaction")
                {
                    if (Context.Context.UserContext.HasPrivilege("InvoiceUndoPostTransaction"))
                        return;
                }
            }

            if (controller == "ExternalParty")
                controller = filterContext.HttpContext.Request.Query["type"];

            if (action == "Index" && Context.Context.UserContext.HasPrivilege(controller + "View"))
                return;

            if (method == "GET" && (action == "Edit" || action == "Create"))
            {
                foreach (string key in filterContext.HttpContext.Request.Query.Keys)
                {
                    if (key.ToUpper() == "ID")
                    {
                        var strVal = filterContext.HttpContext.Request.Query[key];
                        int id;
                        if (int.TryParse(strVal, out id) == false)
                            filterContext.Result = new UnauthorizedResult();

                        if (id == 0)
                        {
                            if (Context.Context.UserContext.HasPrivilege(controller + "Add"))
                                return;

                            SetAuthError(filterContext);
                            return;
                        }

                        if (Context.Context.UserContext.HasPrivilege(controller + "Edit"))
                            return;

                        SetAuthError(filterContext);
                        return;
                    }
                }

                if (Context.Context.UserContext.HasPrivilege(controller + "Add"))
                    return;

                SetAuthError(filterContext);
                return;
            }
            if (method == "POST" && action == "Edit" || action == "Save" || action == "Create") //Save
            {
                return; //TODO: Handle this properly

                //if (Context.Context.UserContext.HasPrivilege(controller + "Add") || Context.Context.UserContext.HasPrivilege(controller + "Edit"))
                //    return;

                //SetAuthError(filterContext);
                //return;
            }

            if ((method == "POST" || method == "GET") && action == "UploadFile")
            {
                if (Context.Context.UserContext.HasPrivilege(controller + "View"))
                    return;
            }

            if (Context.Context.UserContext.HasPrivilege(controller + action))
                return;

            SetAuthError(filterContext);
        }

        private void SetAuthError(AuthorizationFilterContext filterContext)
        {
            var redirectTargetDictionary = new RouteValueDictionary();
            redirectTargetDictionary.Add("action", "AuthenticationError");
            redirectTargetDictionary.Add("controller", "DashBoard1");
            redirectTargetDictionary.Add("error", "You don`t have permission to do this action, Please contact Your Administrator !");
            filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
        }
    }

    public class IgnoreAction : ActionFilterAttribute
    {
        public bool Ignore { get; set; }
    }

    namespace Microsoft.AspNetCore.Mvc
    {
        public static class HelperExtensions
        {
            public static string GetRequiredString(this RouteData routeData, string keyName)
            {
                object value;
                if (!routeData.Values.TryGetValue(keyName, out value))
                {
                    throw new InvalidOperationException($"Could not find key with name '{keyName}'");
                }

                return value?.ToString();
            }
        }
    }
}