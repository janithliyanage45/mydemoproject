﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ERP.Core.Entities;

namespace ERP.Web.ViewModels
{
    public class UserListViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string RoleName { get; set; }
        public bool Active { get; set; }
        public bool Locked { get; set; }
        public virtual string PdaImei { get; set; }

        public override void InitMappings(AutoMapper.Profile profile)
        {
            profile.CreateMap<User, UserListViewModel>();
        }
    }
}