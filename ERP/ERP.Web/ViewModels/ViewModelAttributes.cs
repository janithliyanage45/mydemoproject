﻿using System;

namespace ERP.Web.ViewModels
{
    public class InputAttribute : Attribute
    {
        public string Class { get; set; }
        public string Id { get; set; }
    }

    public class HiddenAttribute : Attribute
    {

    }

    public class TextBoxAttribute : InputAttribute
    {
        public TextBoxAttribute()
        {

        }

        public TextBoxAttribute(int cols)
        {

        }

    }

    public class PasswordAttribute : InputAttribute
    {
        public PasswordAttribute()
        {

        }

        public PasswordAttribute(int cols)
        {

        }

    }

    public class TextAreaAttribute : InputAttribute
    {
        public TextAreaAttribute()
        {

        }

        public TextAreaAttribute(int cols)
        {

        }
    }

    public class RowsAttribute : Attribute
    {
        public RowsAttribute(int rows)
        {

        }
    }

    public class DropDownListAttribute : InputAttribute
    {

    }

    public class BeginGroupAttribute : Attribute
    {

    }

    public class EndGroupAttribute : Attribute
    {

    }

    public class GroupAttribute : Attribute
    {

    }

    public class LabelAttribute : InputAttribute
    {
        public LabelAttribute()
        {

        }

        public LabelAttribute(int cols)
        {

        }
    }

    public class EmptyLabelAttribute : InputAttribute
    {
        public EmptyLabelAttribute()
        {

        }

        public EmptyLabelAttribute(int cols)
        {

        }
    }

    public class CheckBoxAttribute : InputAttribute
    {
        public CheckBoxAttribute()
        {

        }

        public CheckBoxAttribute(int cols)
        {

        }
    }

    public class DropDownAttribute : InputAttribute
    {
        public DropDownAttribute()
        {

        }

        public DropDownAttribute(int cols)
        {

        }
    }

    public class DatePickerAttribute : InputAttribute
    {
        public DatePickerAttribute()
        {

        }

        public DatePickerAttribute(int cols)
        {

        }

    }

    public class MultiSelectAttribute : Attribute
    {

    }

    public class HelpTextAttribute : Attribute
    {
        public HelpTextAttribute(string text)
        {

        }
    }
}
