﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ERP.Core.Entities;

namespace ERP.Web.ViewModels
{
    public class SysConfigViewModel
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string DisplayName { get; set; }

        public virtual string Category { get; set; }

        public virtual SystemParameterType ValueType { get; set; }

        public string Value { get; set; }

        public string EffectiveName
        {
            get { return string.IsNullOrEmpty(DisplayName) ? Name : DisplayName; }
        }
    }

    public class SysConfigListViewModel
    {
        public string FieldNames { get; set; }
        public Dictionary<string, List<SysConfigViewModel>> Value { get; set; }
    }
}