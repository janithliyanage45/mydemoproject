﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ERP.Web.ViewModels
{
    public abstract class ViewModelBase
    {
        public abstract void InitMappings(Profile profile);
    }
}
