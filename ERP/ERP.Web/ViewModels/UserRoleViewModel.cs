﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using ERP.Core.Entities;
using NHibernate.Linq;

namespace ERP.Web.ViewModels
{
    public class UserRoleViewModel
    {
        public UserRoleViewModel()
        {
            ReportPrivileges = new Dictionary<string, List<ReportPrivilegeViewModel>>();
            DynamicReportPrivileges = new List<ReportPrivilegeViewModel>();
        }

        public virtual int Id { get; set; }

        [Required]
        public virtual string Name { get; set; }

        [Display(Name = "Super User (Can do everything)")]
        public virtual bool IsSuperUser { get; set; }

        public IDictionary<string, UserRolePrivilegeGroupViewModel> PrivilegeGroups;

        public IDictionary<string, List<ReportPrivilegeViewModel>> ReportPrivileges { get; set; }

        public IList<ReportPrivilegeViewModel> DynamicReportPrivileges { get; set; }

        public virtual void Init(SecurityPrivileges privileges)
        {
            PrivilegeGroups = new Dictionary<string, UserRolePrivilegeGroupViewModel>();

            var props = typeof(SecurityPrivileges).GetProperties();
            foreach (var p in props)
            {
                if (!p.IsDefined(typeof(PrivilegeAttribute), false))
                    continue;

                //var privAttr = (PrivilegeAttribute) p.GetCustomAttributes(typeof (PrivilegeAttribute), false).First();
                var categoryAttr = (CategoryAttribute)p.GetCustomAttributes(typeof(CategoryAttribute), false).First();
                var displayNameAttr = (DisplayNameAttribute)p.GetCustomAttributes(typeof(DisplayNameAttribute), false).First();
                var value = (bool)p.GetValue(privileges, null);

                var priv = new UserRolePrivilegeViewModel(p.Name, displayNameAttr.DisplayName, value);

                UserRolePrivilegeGroupViewModel pgvm = null;
                if (PrivilegeGroups.ContainsKey(categoryAttr.Category))
                    pgvm = PrivilegeGroups[categoryAttr.Category];
                else
                {
                    pgvm = new UserRolePrivilegeGroupViewModel(categoryAttr.Category);
                    PrivilegeGroups[pgvm.Name] = pgvm;
                }

                pgvm.Privileges.Add(priv);
            }
        }
    }

    public class UserRolePrivilegeGroupViewModel
    {
        public string Name { get; set; }
        public string HtmlName { get { return Name.ToLower().Replace(" ", "-"); } }
        public IList<UserRolePrivilegeViewModel> Privileges;

        public UserRolePrivilegeGroupViewModel(string name)
        {
            Name = name;
            Privileges = new List<UserRolePrivilegeViewModel>();
        }
    }

    public class UserRolePrivilegeViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }

        public UserRolePrivilegeViewModel(string code, string name, bool isSelected)
        {
            Code = code;
            Name = name;
            IsSelected = isSelected;
        }
    }

    public class ReportPrivilegeViewModel
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Category { get; set; }
        public bool Value { get; set; }
        public bool IsDynamic { get; set; }
        public string DisplayName
        {
            get { return Regex.Replace(Name, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0"); }
        }
    }
}
