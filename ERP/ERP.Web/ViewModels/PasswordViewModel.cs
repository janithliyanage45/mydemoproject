﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.Web.ViewModels
{
    public class PasswordViewModel
    {
        [Required]
        [Display(Name = "Current Password")]
        public string CurruntPassword { get; set; }

        [Required]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Required]
        [Display(Name = "Confirm New Password")]
        [Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }

        public int Id { get; set; }

        public bool IsError { get; set; }

        public string Message { get; set; }

        public string Class { get; set; }
    }
}