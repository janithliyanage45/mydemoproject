﻿using System;
using AutoMapper;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.Web.ViewModels
{
    public class ProductViewModel : ViewModelBase
    {
        [Hidden]
        public int Id { get; set; }
        [Required]
        [DisplayName("Product Code")]
        public string Code { get; set; }
        [Required]
        [DisplayName("Product Name")]
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }
        public DateTime? CreatedDate { get; set; }

        public override void InitMappings(Profile profile)
        {
        }
    }
}
