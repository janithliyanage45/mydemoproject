﻿namespace ERP.Web.ViewModels
{
    public class ProductListViewModel
    {
        public int Id { get; internal set; }
        public bool Active { get; internal set; }
        public string Code { get; internal set; }
        public string Name { get; internal set; }
    }
}
