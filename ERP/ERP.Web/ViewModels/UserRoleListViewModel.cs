﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ERP.Core.Entities;

namespace ERP.Web.ViewModels
{
    public class UserRoleListViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSuperUser { get; set; }

        public override void InitMappings(AutoMapper.Profile profile)
        {
            profile.CreateMap<UserRole, UserRoleListViewModel>();
        }
    }
}
