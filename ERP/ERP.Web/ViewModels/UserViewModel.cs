﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;
using ERP.Core.Entities;

namespace ERP.Web.ViewModels
{
    public class UserViewModel : ViewModelBase
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Username { get; set; }

        public string Name { get; set; }

        public string MobileNo { get; set; }

        public string EMail { get; set; }
        public virtual string Address { get; set; }
        public virtual string NICNo { get; set; }

        [DisplayName("Role")]
        public int RoleId { get; set; }

        public bool Active { get; set; }
        public bool Locked { get; set; }

        public bool HandlesPurchasing { get; set; }
        public bool HandlesSales { get; set; }

        public string AllowedBranches { get; set; }

        [DisplayName("Allowed Branches")]
        public int[] AllowedBranchId { get; set; }

        [DisplayName("Tags")]
        public int[] TagsId { get; set; }

        public MultiSelectList TagIdList { get; set; }

        [DisplayName("Task")]
        public int[] TasksId { get; set; }

        public MultiSelectList TaskIdList { get; set; }

        [DisplayName("Restrict Login Times")]
        public bool RestrictLoginTimes { get; set; }

        [DisplayName("Login Time Policy")]
        public int? LoginTimePolicyId { get; set; }

        public string MachineToken { get; set; }

        [DisplayName("Set password")]
        public bool SetPassword { get; set; }

        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm New Password")]
        [Compare("NewPassword")]
        public string RepeatPassword { get; set; }

        public SelectList RoleIdList { get; set; }
        public MultiSelectList AllowedBranchIdList { get; set; }
        public SelectList LoginTimePolicyIdList { get; set; }

        [DisplayName("Supervisor")]
        public int? SupervisorId { get; set; }
        public SelectList SupervisorList { get; set; }

        [Display(Name = "Sales Serial Prefix")]
        public virtual string SalesSerialPrefix { get; set; }
        public int LastSalesNo { get; set; }

        [Display(Name = "Return Serial Prefix")]
        public virtual string ReturnSerialPrefix { get; set; }
        public int LastReturnNo { get; set; }

        [Display(Name = "Last Payment Serial Prefix")]
        public virtual string LastPaymentSerialPrefix { get; set; }
        public int LastPaymentSerialNo { get; set; }

        [DisplayName("Stock Location")]
        public int? StockLocationId { get; set; }
        public SelectList StockLocationList { get; set; }

        [DisplayName("Sales Location")]
        public int? SalesLocationId { get; set; }
        public SelectList SalesLocationList { get; set; }

        [DisplayName("Normal Return Location")]
        public int? NormalReturnLocationId { get; set; }
        public SelectList NormalReturnLocationList { get; set; }

        [DisplayName("Damaged Return Location")]
        public int? DamagedReturnLocationId { get; set; }
        public SelectList DamagedReturnLocationList { get; set; }

        [DisplayName("Territory")]
        public int? TerritoryId { get; set; }
        public SelectList TerritoryList { get; set; }

        [DisplayName("Sub Territory")]
        public int? SubTerritoryId { get; set; }
        public SelectList SubTerritoryList { get; set; }

        [DisplayName("Rep Cash In Hand Account")]
        public int? RepCashInHandAccountId { get; set; }
        public SelectList RepCashInHandAccountList { get; set; }

        [DisplayName("Rep Cheque In Hand Account")]
        public int? RepChequeInHandAccountId { get; set; }
        public SelectList RepChqueInHandAccountList { get; set; }

        [DisplayName("Rep Account")]
        public int? RepAccountId { get; set; }
        public SelectList RepAccountList { get; set; }

        [Display(Name = "Image Serial Prefix")]
        public virtual string ImageSerialPrefix { get; set; }
        public int LastImageNo { get; set; }

        [Display(Name = "Return Request Serial Prefix")]
        public virtual string ReturnRequestSerialPrefix { get; set; }
        public int LastReturnRequestNo { get; set; }

        [Display(Name = "Consumption Journal Serial Prefix")]
        public virtual string ConsumptionJournalSerialPrefix { get; set; }
        public int LastConsumptionJournalNo { get; set; }

        [Display(Name = "Call Lost Prefix")]
        public virtual string CallLostSerialPrefix { get; set; }
        public int LastCallLostSerialNo { get; set; }

        [Display(Name = "Servay Prefix")]
        public virtual string ServaySerialPrefix { get; set; }
        public int LastServayNo { get; set; }

        [Display(Name = "Competitor Stock Taking Servay Prefix")]
        public virtual string CompetitorStockTakingSerialPrefix { get; set; }
        public int LastCompetitorStockTakingNo { get; set; }

        [Display(Name = "Stock Taking Servay Prefix")]
        public virtual string StockTakingSerialPrefix { get; set; }
        public int LastStockTakingNo { get; set; }

        [Display(Name = "Imei No")]
        public virtual string PdaImei { get; set; }

        public bool EnableSFA { get; set; }

        [Display(Name = "IsInherit")]
        public bool IsInherit { get; set; }

        [Display(Name = "Track Rep After Duty Off")]
        public bool TrackRepAfterDutyOff { get; set; }

        [Display(Name = "Credit Approval Request Authorize")]
        public bool CreditApprovalRequest { get; set; }

        [DisplayName(@"Type")]
        public virtual int TypeId { get; set; }
        public SelectList TypeIdList { get; set; }

        [Display(Name = "Last Sales Order Serial Prefix")]
        public virtual string SaleOrderSerialPrefix { get; set; }
        public int LastSaleOrderSerialNo { get; set; }

        [DisplayName("Category")]
        public int? UserCategoryId { get; set; }
        public SelectList UserCategoryList { get; set; }

        [DisplayName("Invoice Template")]
        public int? InvoiceTemplateId { get; set; }
        public SelectList InvoiceTemplateList { get; set; }

        public decimal CreditLimit { get; set; }

        public int CreditDays { get; set; }

        [DisplayName("Region")]
        public int? RegionId { get; set; }

        [DisplayName(@"Department")]
        public int? SubAnalysisId { get; set; }
        public SelectList SubAnalysisIdList { get; set; }

        [DisplayName(@"Allowed Distributor")]
        public int? AllowedDistributorId { get; set; }
        public SelectList AllowedDistributorIdList { get; set; }

        [DisplayName(@"Zone")]
        public int? ZoneId { get; set; }
        public SelectList ZoneIdList { get; set; }

        public SelectList RegionIdList { get; set; }

        public virtual bool IsSyncUser { get; set; }

        public virtual bool IsSelectZone { get; set; }

        [DisplayName(@"SMS Broadcasting Numbers")]
        public string SMSBroadcastingNumbers { get; set; }

        [DisplayName(@"Allow Switching Branches")]
        public bool AllowSwitchingBranches { get; set; }

        public string Description { get; set; }
        public virtual int? CompanyId { get; set; }
        public int? SalesHierarchyNodeId { get; set; }
        public SelectList NodeIdList { get; set; }
        public SelectList CompanyIdList { get; set; }

        public string ImageKey { get; set; }

        public virtual string StaffNo { get; set; }

        public bool LoginWithAD { get; set; }

        public decimal BasicSallary { get; set; }

        public override void InitMappings(AutoMapper.Profile profile)
        {
            profile.CreateMap<User, UserViewModel>()
                .ReverseMap()
                .ForMember(x => x.Type, y => y.Ignore())
                .ForMember(x => x.Role, y => y.Ignore())
                .ForMember(x => x.Supervisor, y => y.Ignore());
        }

        public void Init()
        {
            RoleIdList = new SelectList(DAO<UserRole>.ListAll(), "Id", "Name");
            var branches = new List<BasicEntity>();

            RegionIdList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            var initialSelection = new int[] { };
            if (AllowedBranchId == null && !string.IsNullOrEmpty(AllowedBranches))
                initialSelection = AllowedBranches.Split(',').Select(int.Parse).ToArray();

            AllowedBranchIdList = new MultiSelectList(branches, "Id", "Code", initialSelection);

            LoginTimePolicyIdList = new SelectList(new List<BasicEntity>(), "Id", "Name");

            SupervisorList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            UserCategoryList = new SelectList(new List<BasicEntity>(), "Id", "Name");

            InvoiceTemplateList = new SelectList(new List<BasicEntity>(), "Id", "Code");

            StockLocationList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            SalesLocationList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            NormalReturnLocationList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            DamagedReturnLocationList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            SubAnalysisIdList = new SelectList(new List<BasicEntity>(), "Id", "Name");

            RepCashInHandAccountList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            RepChqueInHandAccountList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            RepAccountList = new SelectList(new List<BasicEntity>(), "Id", "Name");
            NodeIdList = new SelectList(new List<BasicEntity>(), "Id", "Name");

            AllowedDistributorIdList = new SelectList(new List<BasicEntity>(), "Id", "Name");

            var typeSelectItemList = new List<SelectListItem>();
            var index = 0;
            foreach (UserType val in Enum.GetValues(typeof(UserType)))
            {
                var item = new SelectListItem() { Value = index.ToString(), Text = val.ToString(), Selected = false };
                typeSelectItemList.Add(item);
                index++;
            }

            TypeIdList = new SelectList(typeSelectItemList, "Value", "Text");

            var companies = new List<BasicEntity>();
            User user = Context.Context.UserContext.UserInfo;
            companies = new List<BasicEntity>();

            CompanyIdList = new SelectList(companies, "Id", "Name");
        }
    }

    public class BasicEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}