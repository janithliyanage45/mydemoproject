﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ERP.Core.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.Web.ViewModels
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Imei { get; set; }
        public int BranchId { get; set; }
        public string SubName { get; set; }
        public string VersionInfo { get; set; }

        public SelectList BranchIdList { get; set; }

        public void Init()
        {
            var branches = new List<BasicEntity>();
            BranchIdList = new SelectList(branches, "Id", "Name");

            var subName = SysConfig.FindValueByName("BranchName");
            SubName = subName;

            var versionInfo = DAO<VersionInfo>.ListAll().FirstOrDefault();
            VersionInfo = versionInfo.Version;
        }
    }
}