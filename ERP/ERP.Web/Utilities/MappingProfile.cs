﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using ERP.Core.Entities;

using ERP.Web.ViewModels;

namespace Sfa.Web.Utils
{
    public class MappingProfile : Profile
    {
        //public static IMapper Mapper { get; private set; }

        public static void Init()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            Globals.Mapper = mappingConfig.CreateMapper();
        }

        public MappingProfile()
        {
            InitAutoMappings();
        }

        private void InitAutoMappings()
        {
            var baseType = typeof(ViewModelBase);
            var types = Assembly.GetExecutingAssembly().GetExportedTypes();
            foreach (var type in types)
            {
                if (type.Namespace != "ERP.Web.ViewModels")
                    continue;

                if (!type.IsSubclassOf(baseType))
                    continue;

                var obj = (ViewModelBase)Activator.CreateInstance(type);
                obj.InitMappings(this);
            }
        }
    }


}
