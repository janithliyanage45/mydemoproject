﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using NHibernate;

namespace ERP.Core.Entities
{
    public class Product : EntityBase
    {
        
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal Price { get; set; }
        public virtual bool Active { get; set; }
        public virtual DateTime CreatedDate { get; set; }
    }
}


namespace ERP.Core.Entities.Mapping
{
    public class ProductMap : ClassMap<Product>
    {
        public ProductMap()
        {
            Table("products");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Code);
            Map(x => x.Name);
            Map(x => x.Price);
            Map(x => x.Active);
            Map(x => x.CreatedDate);
        }
    }    
}