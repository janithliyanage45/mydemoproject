﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ERP.Core.Entities
{
    public static class Globals
    {
        public static User CurrentUser { get; set; }
        public static IConfiguration AppSettings { get; set; }
        public static IMapper Mapper { get; set; }
        public static IHostingEnvironment HostingEnvironment { get; set; }
        public static SysConfig SysConfigs { get; set; }

    }
}