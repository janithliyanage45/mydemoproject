using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Mapping;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Criterion;

namespace ERP.Core.Entities
{
    public enum UserType
    {
        General,
        Rep,
        Cashier,
        Technician,
        Supervisor,
        External

    }

    public class User : EntityBase
    {
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string MobileNo { get; set; }
        public virtual string EMail { get; set; }
        public virtual string Address { get; set; }
        public virtual string NICNo { get; set; }
        public virtual UserRole Role { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool Locked { get; set; }
        public virtual int FailedLoginAttempts { get; set; }
        public virtual DateTime LockedDate { get; set; }
        //public virtual string AllowedBranches { get; set; }
        public virtual bool RestrictLoginTimes { get; set; }
        public virtual string MachineToken { get; set; }

        public virtual bool HandlesPurchasing { get; set; }
        public virtual bool HandlesSales { get; set; }
        public virtual string PdaImei { get; set; }
        public virtual int LastInvoiceId { get; set; }
        public virtual string SalesSerialPrefix { get; set; }
        public virtual int LastSalesNo { get; set; }
        public virtual string ReturnSerialPrefix { get; set; }
        public virtual int LastReturnNo { get; set; }
        public virtual string LoadingSerialPrefix { get; set; }
        public virtual int LastLoadingSerialNo { get; set; }
        public virtual string UnloadingSerialPrefix { get; set; }
        public virtual int LastUnloadingSerialNo { get; set; }
        public virtual string CallLostSerialPrefix { get; set; }
        public virtual int LastCallLostSerialNo { get; set; }
        public virtual string PromoIssueSerialPrefix { get; set; }
        public virtual int LastPromoIssueSerialNo { get; set; }
        public virtual int LastPaymentSerialNo { get; set; }
        public virtual string LastPaymentSerialPrefix { get; set; }
        public virtual string SaleOrderSerialPrefix { get; set; }
        public virtual int LastSaleOrderSerialNo { get; set; }
        public virtual string ConsumptionJournalSerialPrefix { get; set; }
        public virtual int LastConsumptionJournalNo { get; set; }
        public virtual string ServaySerialPrefix { get; set; }
        public virtual int LastServayNo { get; set; }
        public virtual string CompetitorStockTakingSerialPrefix { get; set; }
        public virtual int LastCompetitorStockTakingNo { get; set; }

        public virtual string StockTakingSerialPrefix { get; set; }
        public virtual int LastStockTakingNo { get; set; }

        public virtual string LoanReturnSerialPrefix { get; set; }
        public virtual int LastLoanReturnNo { get; set; }

        public virtual string Description { get; set; }

        public virtual string ImageSerialPrefix { get; set; }
        public virtual int LastImageNo { get; set; }
        public virtual string ReturnRequestSerialPrefix { get; set; }
        public virtual int LastReturnRequestNo { get; set; }

        public virtual string CommonSerialPrefix { get; set; }
        public virtual int LastCommonNo { get; set; }

        public virtual string LoadingRequestSerialPrefix { get; set; }
        public virtual int LastLoadingRequestNo { get; set; }
        public virtual User Supervisor { get; set; }

        public virtual bool EnableSFA { get; set; }
        public virtual bool IsEvisionUser { get; set; }
        public virtual int LastCustomerId { get; set; }
        public virtual UserType Type { get; set; }
        public virtual DateTime LastConnectedDateTime { get; set; }
        public virtual DateTime LastDownLoadComletedDateTime { get; set; }
        public virtual int Version { get; set; }
        public virtual bool IsInherit { get; set; }

        public virtual bool CreditApprovalRequest { get; set; }

        public virtual int DefaultInvoiceTemplate { get; set; }


        public virtual bool TrackRepAfterDutyOff { get; set; }
        public virtual string ClassType { get; set; }
        public virtual bool IsSyncUser { get; set; }


        public virtual bool IsSelectZone { get; set; }

        public virtual decimal CreditLimit { get; set; }
        public virtual int CreditDays { get; set; }
        public virtual string ImageKey { get; set; }
        public virtual string NewPassword { get; set; }
        public virtual string SMSBroadcastingNumbers { get; set; }

        public virtual bool AllowSwitchingBranches { get; set; }

        public virtual string StaffNo { get; set; }

        public virtual bool LoginWithAD { get; set; }

        public virtual decimal BasicSallary { get; set; }


        public virtual void DeepInitialize()
        {
            NHibernateUtil.Initialize(Role);
        }
    }
}

namespace ERP.Core.Entities.Mapping
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("users");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Username);
            Map(x => x.Code);
            Map(x => x.Password);
            Map(x => x.Name);
            Map(x => x.MobileNo);
            Map(x => x.EMail);
            Map(x => x.Address);
            Map(x => x.NICNo);
            References(x => x.Role).Column("RoleId");
            Map(x => x.Active);
            Map(x => x.Locked);
            Map(x => x.FailedLoginAttempts);
            Map(x => x.LockedDate);
            Map(x => x.RestrictLoginTimes).Not.Nullable();
            Map(x => x.MachineToken);
            Map(x => x.HandlesPurchasing);
            Map(x => x.HandlesSales);
            Version(x => x.Version);
            Map(x => x.PdaImei);
            Map(x => x.LastInvoiceId);
            Map(x => x.SalesSerialPrefix);
            Map(x => x.LastSalesNo);
            Map(x => x.ReturnSerialPrefix);
            Map(x => x.LastReturnNo);
            Map(x => x.LoadingSerialPrefix);
            Map(x => x.LastLoadingSerialNo);
            Map(x => x.UnloadingSerialPrefix);
            Map(x => x.LastUnloadingSerialNo);
            Map(x => x.LastCallLostSerialNo);
            Map(x => x.CallLostSerialPrefix);
            Map(x => x.LastPromoIssueSerialNo);
            Map(x => x.PromoIssueSerialPrefix);
            Map(x => x.LastPaymentSerialPrefix);
            Map(x => x.LastSaleOrderSerialNo);
            Map(x => x.SaleOrderSerialPrefix);
            Map(x => x.ServaySerialPrefix);
            Map(x => x.LastServayNo);
            Map(x => x.CommonSerialPrefix);
            Map(x => x.LastCommonNo);
            Map(x => x.StaffNo);

            Map(x => x.CompetitorStockTakingSerialPrefix);
            Map(x => x.LastCompetitorStockTakingNo);

            Map(x => x.StockTakingSerialPrefix);
            Map(x => x.LastStockTakingNo);

            Map(x => x.ImageSerialPrefix);
            Map(x => x.LastImageNo);
            Map(x => x.ReturnRequestSerialPrefix);
            Map(x => x.LastReturnRequestNo);

            DiscriminateSubClassesOnColumn("ClassType");

            Map(x => x.LastPaymentSerialNo);
            Map(x => x.LastCustomerId);
            References(x => x.Supervisor);
            Map(x => x.IsEvisionUser);
            Map(x => x.EnableSFA);
            Map(x => x.Description);
            Map(x => x.LastConnectedDateTime);
            Map(x => x.LastDownLoadComletedDateTime);
            Map(x => x.LastConsumptionJournalNo);
            Map(x => x.ConsumptionJournalSerialPrefix);
            Map(x => x.CreditApprovalRequest);
            Map(x => x.Type).CustomType<UserType>();

            Map(x => x.DefaultInvoiceTemplate);
            Map(x => x.IsInherit);
            Map(x => x.IsSyncUser).Not.Nullable().Default("0");
            Map(x => x.TrackRepAfterDutyOff);
            Map(x => x.IsSelectZone);

            Map(x => x.LoadingRequestSerialPrefix);
            Map(x => x.LastLoadingRequestNo);

            Map(x => x.CreditLimit);
            Map(x => x.CreditDays);

            Map(x => x.LoanReturnSerialPrefix);
            Map(x => x.LastLoanReturnNo);
            Map(x => x.SMSBroadcastingNumbers);

            Map(x => x.ImageKey);

            Map(x => x.AllowSwitchingBranches).Not.Nullable().Default("0");

            Map(x => x.LoginWithAD).Not.Nullable().Default("0");
        }
    }
}
