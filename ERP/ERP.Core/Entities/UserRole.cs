using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using FluentNHibernate.Data;
using FluentNHibernate.Mapping;
using Iesi.Collections.Generic;
using Newtonsoft.Json;
using NHibernate;

namespace ERP.Core.Entities
{
    public class UserRole : EntityBase
    {
        public virtual string Name { get; set; }
        public virtual bool IsSuperUser { get; set; }

        [JsonIgnore]
        public virtual ISet<RolePrivilege> Privileges { get; set; }

        public virtual SecurityPrivileges GetPrivileges()
        {
            var secPriv = new SecurityPrivileges() { RoleId = Id, IsSuperUser = IsSuperUser };

            PropertyInfo[] props = typeof(SecurityPrivileges).GetProperties();
            foreach (PropertyInfo p in props)
            {
                if (!p.IsDefined(typeof(PrivilegeAttribute), false))
                    continue;

                //var privAttr = p.GetCustomAttributes(typeof(PrivilegeAttribute), false).FirstOrDefault() as PrivilegeAttribute;

                var priv = Privileges.FirstOrDefault(x => x.Code == p.Name);
                if (priv == null)
                    continue; //Skip undefined privileges

                p.SetValue(secPriv, priv.Value, null);
            }

            return secPriv;
        }

        public virtual void ApplyPrivileges(SecurityPrivileges secPriv)
        {
            PropertyInfo[] props = typeof(SecurityPrivileges).GetProperties();
            foreach (PropertyInfo p in props)
            {
                if (!p.IsDefined(typeof(PrivilegeAttribute), false))
                    continue;

                //var privAttr = p.GetCustomAttributes(typeof(PrivilegeAttribute), false).FirstOrDefault() as PrivilegeAttribute;

                var priv = Privileges.FirstOrDefault(x => x.Code == p.Name);
                if (priv == null)
                {
                    priv = new RolePrivilege() { Role = this, Code = p.Name };
                    Privileges.Add(priv);
                }

                priv.Value = (bool)p.GetValue(secPriv, null);
            }
        }


        public virtual void DeepInitilize()
        {

        }
    }
}

namespace ERP.Core.Entities.Mapping
{
    public class UserRoleMap : ClassMap<UserRole>
    {
        public UserRoleMap()
        {
            Table("user_roles");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Unique();
            Map(x => x.IsSuperUser).Not.Nullable().Default("0");

            HasMany(x => x.Privileges)
                .KeyColumn("RoleId")
                .AsSet()
                .Cascade.AllDeleteOrphan()
                .Inverse();
        }
    }
}

