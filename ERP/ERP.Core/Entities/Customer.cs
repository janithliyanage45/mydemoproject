﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Core.Entities
{
    public class Customer : EntityBase
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual decimal Outstanding { get; set; }
        public virtual DateTime DOB { get; set; }
        public virtual bool Active { get; set; }

    }
}

namespace ERP.Core.Entities.Mapping
{
    public class CustomerMap : ClassMap<Customer>
    {
        public CustomerMap()
        {
            Table("customers");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Code).Not.Nullable();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Outstanding);
            Map(x => x.DOB).Not.Nullable();
            Map(x => x.Active);

        }
    }
}
