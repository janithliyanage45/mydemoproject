using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Data;
using FluentNHibernate.Mapping;

namespace ERP.Core.Entities
{
    public class RolePrivilege
    {
        public virtual int Id { get; set; }
        public virtual UserRole Role { get; set; }
        public virtual string Code { get; set; }
        public virtual bool Value { get; set; }
    }
}

namespace ERP.Core.Entities.Mapping
{
    public class RolePrivilegeMap : ClassMap<RolePrivilege>
    {
        public RolePrivilegeMap()
        {
            Table("role_privileges");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Role).Not.Nullable();
            Map(x => x.Code).Not.Nullable();
            Map(x => x.Value).Not.Nullable();
        }
    }
}
