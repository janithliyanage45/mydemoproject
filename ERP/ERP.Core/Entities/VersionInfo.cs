using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace ERP.Core.Entities
{
    public class VersionInfo
    {
        public virtual int Id { get; set; }
        public virtual string Version { get; set; }
    }
}

namespace ERP.Core.Entities.Mapping
{
    public class VersionInfoMap : ClassMap<VersionInfo>
    {
        public VersionInfoMap()
        {
            Table("version_info");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Version);
        }
    }
}
