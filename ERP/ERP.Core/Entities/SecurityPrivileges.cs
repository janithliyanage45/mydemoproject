﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace ERP.Core.Entities
{
    public class PrivilegeAttribute : Attribute
    {
    }

    public class SecurityPrivileges
    {
        [Browsable(false)]
        public long RoleId { get; set; }

        public bool IsSuperUser { get; set; }

        #region Menu
        [Privilege, Category("Main Menu"), DisplayName("Hide Dashboards")]
        public bool HideMainMenuMaterData { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide Master Data")]
        public bool HideMainMenuDashboards { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide Sales")]
        public bool HideMainMenuSales { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide Procument")]
        public bool HideMainMenuProcument { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide Inventory")]
        public bool HideMainMenuInventory { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide SFA")]
        public bool HideMainMenuSFA { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide Administration")]
        public bool HideMainMenuAdministration { get; set; }

        [Privilege, Category("Main Menu"), DisplayName("Hide Reports")]
        public bool HideMainMenuReports { get; set; }
        #endregion


        public bool CanPerformAction(string entityCode, string actionCode)
        {
            var fullCode = entityCode + ":" + actionCode;
            return HasPrivilege(fullCode);
        }

        public bool HasPrivilege(string code)
        {
            if (IsSuperUser)
                return true;

            var props = typeof(SecurityPrivileges).GetProperties();
            foreach (var p in props)
            {
                if (!p.IsDefined(typeof(PrivilegeAttribute), false))
                    continue;

                //var privAttr = (PrivilegeAttribute)p.GetCustomAttributes(typeof(PrivilegeAttribute), false).First();
                if (p.Name == code)
                    return (bool)p.GetValue(this, null);

            }

            return true;
            //throw new ArgumentException("Invalid privilege code: " + fullCode);
        }

        public void SetPrivilege(string fullCode, bool value)
        {
            var props = typeof(SecurityPrivileges).GetProperties();
            foreach (var p in props)
            {
                if (!p.IsDefined(typeof(PrivilegeAttribute), false))
                    continue;

                //var privAttr = (PrivilegeAttribute)p.GetCustomAttributes(typeof(PrivilegeAttribute), false).First();
                if (p.Name == fullCode)
                {
                    p.SetValue(this, value, null);
                    return;
                }

            }
        }

        public void ResetAllPrivileges(bool value)
        {
            PropertyInfo[] props = typeof(SecurityPrivileges).GetProperties();
            foreach (PropertyInfo p in props)
            {
                if (!p.IsDefined(typeof(PrivilegeAttribute), false))
                    continue;

                p.SetValue(this, value, null);
            }
        }

    }
}
