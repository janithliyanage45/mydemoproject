﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP.Core.Entities
{
    [Serializable]
    public abstract class EntityBase : IEquatable<EntityBase>
    {
        public virtual int Id { get; set; }

        public static bool operator ==(EntityBase left, EntityBase right)
        {
            return object.Equals((object)left, (object)right);
        }

        public static bool operator !=(EntityBase left, EntityBase right)
        {
            return !object.Equals((object)left, (object)right);
        }

        public virtual bool Equals(EntityBase obj)
        {
            if (object.ReferenceEquals((object)null, (object)obj))
                return false;
            if (object.ReferenceEquals((object)this, (object)obj))
                return true;

            //if ( this.GetType() != obj.GetType())
            //    return false;

            //Check type compatibility
            if (this.GetType() == obj.GetType() || obj.GetType().IsSubclassOf(this.GetType()) || this.GetType().IsSubclassOf(obj.GetType()))
            {
                if (obj.Id == 0 && this.Id == 0)
                    return object.ReferenceEquals((object)this, (object)obj);

                return obj.Id == this.Id;
            }
            else
                return false;
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals((object)null, obj))
                return false;
            if (object.ReferenceEquals((object)this, obj))
                return true;
            //if (this.GetType() != obj.GetType())
            //    return false;
            if (!obj.GetType().IsSubclassOf(typeof(EntityBase)))
                return false;
            else
                return this.Equals((EntityBase)obj);
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode() * 397 ^ this.GetType().GetHashCode();
        }

        public virtual bool IsTransient { get { return Id == 0; } }
    }

    public class UniqueAttribute : Attribute
    {
        
    }
}
