using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using NHibernate;
using NHibernate.Criterion;

namespace ERP.Core.Entities
{
    public enum SystemParameterType
    {
        STRING,
        INT,
        DECIMAL,
        BOOL,
        DATE,
        STRING_LIST,
        INT_LIST
    }

    [Serializable]
    public class SysParam
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual string Category { get; set; }

        public virtual SystemParameterType ValueType { get; set; }

        public virtual string ValueTypeStr
        {
            get
            {
                return ValueType.ToString();
            }
        }

        private string _value;
        public virtual string Value 
        {
            get { return _value; }
            set { _value = value; }
        }

        public virtual int IntValue
        {
            get 
            {
                AssertType(SystemParameterType.INT);
                return int.Parse(_value); 
            }
        }

        public virtual decimal DecimalValue
        {
            get 
            {
                AssertType(SystemParameterType.DECIMAL, SystemParameterType.INT);
                return decimal.Parse(_value); 
            }
        }

        public virtual string StringValue
        {
            get { return _value; }
        }

        public virtual string[] StringListValue
        {
            get { return _value.Split('|'); }
        }

        public virtual int[] IntListValue
        {
            get
            {
                string[] strs = StringListValue;
                int[] result = new int[strs.Length];
                for (int i = 0; i < strs.Length; i++)
                    result[i] = int.Parse(strs[i]);

                return result;
            }
        }

        public virtual DateTime DateValue
        {
            get
            {
                AssertType(SystemParameterType.DATE);
                string[] tokens = _value.Split('/');
                return new DateTime(int.Parse(tokens[2]), int.Parse(tokens[1]), int.Parse(tokens[0]));
            }
        }

        public virtual bool BoolValue
        {
            get 
            {
                AssertType(SystemParameterType.BOOL);
                return bool.Parse(_value);
            }
        }

        public virtual object ObjectValue
        {
            get
            {
                switch (ValueType)
                {
                    case SystemParameterType.STRING:
                        return StringValue;

                    case SystemParameterType.INT:
                        return IntValue;

                    case SystemParameterType.DECIMAL:
                        return DecimalValue;

                    case SystemParameterType.BOOL:
                        return BoolValue;

                    case SystemParameterType.STRING_LIST:
                        return StringListValue;

                    case SystemParameterType.INT_LIST:
                        return IntListValue;
                }

                return null;
            }
        }

        public virtual void AssertType(params SystemParameterType[] types)
        {
            bool bCompatible = false;
            foreach (SystemParameterType t in types)
            {
                if (t == ValueType)
                    bCompatible = true;
            }

            if (!bCompatible)
                throw new InvalidParameterTypeException();
        }

        public static SysParam GetParam(ISession session, string name)
        {
            ICriteria crit = session.CreateCriteria(typeof(SysParam)).Add(Expression.Eq("Name", name));
            IList<SysParam> lst = crit.List<SysParam>();
            if (lst == null || lst.Count == 0)
                return null;

            return lst[0];
        }

        public static SystemParameterType ToSysParamType(Type t)
        {
            if (t == typeof(string))
                return SystemParameterType.STRING;

            if (t == typeof(int))
                return SystemParameterType.INT;

            if (t == typeof(decimal))
                return SystemParameterType.DECIMAL;

            if (t == typeof(bool))
                return SystemParameterType.BOOL;

            if (t == typeof(string[]))
                return SystemParameterType.STRING_LIST;

            if (t == typeof(int[]))
                return SystemParameterType.INT_LIST;

            if (t == typeof(DateTime))
                return SystemParameterType.DATE;

            return SystemParameterType.STRING;
        }

    }

    public class InvalidParameterTypeException : Exception
    {
        public InvalidParameterTypeException()
            :base("Invalid Parameter")
        {

        }
    };
}

namespace ERP.Core.Entities.Mapping
{
    public class SysParamMap : ClassMap<SysParam>
    {
        public SysParamMap()
        {
            Table("system_parameters");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Not.Nullable().Unique();
            Map(x => x.ValueType).CustomType<SystemParameterType>();
            Map(x => x.Value);
        }
    }
}
