using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace ERP.Core.Entities
{
    public class AuditTrailEntry
    {
        public virtual Guid Id { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual int UserId { get; set; }
        public virtual string Username { get; set; }
        public virtual string Category { get; set; }
        public virtual string Action { get; set; }
        public virtual string Message { get; set; }
    }
}

namespace ERP.Core.Entities.Mapping
{
    public class AuditTrailEntryMap : ClassMap<AuditTrailEntry>
    {
        public AuditTrailEntryMap()
        {
            Table("audit_trail_entries");
            Id(x => x.Id).GeneratedBy.Guid().CustomType<string>().Length(50);
            Map(x => x.Date).Not.Nullable();
            Map(x => x.UserId);
            Map(x => x.Username);
            Map(x => x.Category);
            Map(x => x.Action);
            Map(x => x.Message);
        }
    }
}
