﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using Expression = NHibernate.Criterion.Expression;
using NHibernate.Linq;

namespace ERP.Core.Entities
{
    public class DAO
    {
        public static void Save<T>(T obj)
        {
            DAO<T>.Save(obj);
        }
    }

    public class DAO<T>
    {
        public static T Load(object id)
        {
            using (new DefaultSessionScope())
                return SessionScope.CurrentSession.Load<T>(id);
        }

        public static T Get(object id)
        {
            using (new DefaultSessionScope())
                return SessionScope.CurrentSession.Get<T>(id);
        }

        public static T FindByProperty(string property, object value)
        {
            using (new DefaultSessionScope())
            {
                IList<T> lst = SessionScope.CurrentSession.CreateCriteria(typeof(T))
                    .Add(Expression.Eq(property, value))
                    .List<T>();

                if (lst == null || lst.Count == 0)
                    return default(T);

                return lst[0];                
            }

        }

        public static IList<T> FindAllByProperty(string property, object value)
        {
            using (new DefaultSessionScope())
                return SessionScope.CurrentSession.CreateCriteria(typeof(T))
                    .Add(Expression.Eq(property, value))
                    .List<T>();
        }

        public static IList<T> ListAll()
        {
            using (new DefaultSessionScope())
                return SessionScope.CurrentSession.CreateCriteria(typeof(T)).List<T>();
        }

        public static void Save(T obj)
        {
            using (new DefaultSessionScope())
            using (ITransaction txn = SessionScope.CurrentSession.BeginTransaction())
            {
                SessionScope.CurrentSession.SaveOrUpdate(obj);
                txn.Commit();
            }
        }

        public static void Update(T obj)
        {
            using (new DefaultSessionScope())
            using (ITransaction txn = SessionScope.CurrentSession.BeginTransaction())
            {
                SessionScope.CurrentSession.Update(obj);
                txn.Commit();
            }
        }

        public static void Delete(T obj)
        {
            using (new DefaultSessionScope())
            using (ITransaction txn = SessionScope.CurrentSession.BeginTransaction())
            {
                SessionScope.CurrentSession.Delete(obj);
                txn.Commit();
            }
        }

        public static void DeleteById(object id)
        {
            using (new DefaultSessionScope())
            using (ITransaction txn = SessionScope.CurrentSession.BeginTransaction())
            {
                var obj = SessionScope.CurrentSession.Load<T>(id);
                SessionScope.CurrentSession.Delete(obj);
                txn.Commit();
            }
        }

        public static T FirstOrDefault(Func<T, bool> func)
        {
            using (new DefaultSessionScope())
            {
                return SessionScope.CurrentSession.Query<T>()
                    .FirstOrDefault(func);
            }
        }

        public static IList<T> FindAll(Func<T, bool> func)
        {
            using (new DefaultSessionScope())
            {
                return SessionScope.CurrentSession.Query<T>()
                    .Where(func)
                    .ToList();
            }
        }

        public static int GetCount()
        {
            using (new DefaultSessionScope())
            {
                return SessionScope.CurrentSession.Query<T>()
                    .Count();
            }
        }
    }
}
