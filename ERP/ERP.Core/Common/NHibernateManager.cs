using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ERP.Core.Entities;
using NHibernate;
using NHibernate.Cfg;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Helpers;
using System.Reflection;
using FluentNHibernate.Conventions.Instances;
using NLog;
using NHibernate.Type;
using System.Diagnostics;

namespace ERP
{
    public class NHibernateManager
    {
        static ISessionFactory factory = null;
        static ISessionFactory reportingFactory = null;

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public static string ConnStr { get; set; }
        public static string ReportingConnStr { get; set; }

        private static FluentConfiguration GetConfiguration(string connStr)
        {

            var c = connStr;

            var config = Fluently.Configure().Database(
                MySQLConfiguration.Standard.ConnectionString(connStr.StartsWith("*") ? new DocMan().BB(connStr) : connStr)
                    .Dialect<MySQL5Dialect>()
                //.ShowSql()
                )
                .Mappings(m =>
                    {
                        var ass = m.FluentMappings.AddFromAssemblyOf<NHibernateManager>();

                        ass.Conventions.Add(ForeignKey.EndsWith("Id"),
                                                       new ReferenceConvention());

                        m.HbmMappings.AddFromAssemblyOf<NHibernateManager>();
                    }
                );

            config.ExposeConfiguration(x =>
            {
                x.SetInterceptor(new SqlStatementInterceptor());
            });

            return config;
        }

        private static void InitFluent()
        {
            try
            {
                factory = GetConfiguration(ConnStr).BuildSessionFactory();
            }
            catch (System.Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                else
                    throw;
            }
        }

        private static void InitReportingFluent()
        {
            try
            {
                reportingFactory = GetConfiguration(ReportingConnStr).BuildSessionFactory();
            }
            catch (System.Exception ex)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                else
                    throw;
            }
        }


        public static void CreateDatabase()
        {
            CreateDatabase(BuildSchema);

            // EntityManager.CreateInitialEntities();
        }

        public static void GenerateCreationScipt()
        {
            CreateDatabase(BuildSchemaFile);
        }

        public static void CreateDatabase(Action<Configuration> schemaCfg)
        {
            try
            {
                factory = GetConfiguration(ConnStr)
                        .ExposeConfiguration(schemaCfg)
                        .BuildSessionFactory();

            }
            catch (IOException ex)
            {
                //MessageBox.Show("Error: \n" + Utils.GetExceptionText(ex), "Error",
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
            }
        }

        private static void BuildSchema(Configuration cfg)
        {
            new SchemaExport(cfg).SetOutputFile("erp.sql").Create(false, true);
        }

        private static void BuildSchemaFile(Configuration cfg)
        {
            new SchemaExport(cfg).SetOutputFile("erp.sql").Create(true, false);
        }

        public static ISessionFactory GetSessionFactory()
        {
            if (factory == null)
                InitFluent();

            return factory;
        }

        public static ISessionFactory GetReportingSessionFactory()
        {
            if (reportingFactory == null)
                InitReportingFluent();

            return reportingFactory;
        }


        public static void CheckAndInitialize(object proxy)
        {
            if (proxy == null)
                return;

            if (!NHibernateUtil.IsInitialized(proxy))
                NHibernateUtil.Initialize(proxy);
        }
    }

    public class ReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            instance.ForeignKey(string.Format("FK_{0}_{1}_ID",
                 instance.EntityType.Name,
                 instance.Name));
        }
    }


    public class SessionScope : IDisposable
    {
        [ThreadStatic]
        private static SessionScope _current = null;

        public static SessionScope Current
        {
            get { return _current; }
        }

        private ISession _session = null;
        public static ISession CurrentSession
        {
            get { return SessionScope.Current._session; }
        }

        public SessionScope()
        {
            if (_current != null)
                throw new Exception("Session scope already active");
          //  _session = NHibernateManager.GetSessionFactory().OpenSession(new AuditInterceptor());
            _session = NHibernateManager.GetSessionFactory().OpenSession();
            _current = this;
        }

        public SessionScope(FlushMode flushMode)
            : this()
        {
            _session.FlushMode = flushMode;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_session != null)
            {
                _session.Dispose();
                _session = null;
            }

            _current = null;
        }

        #endregion
    }

    public class ReportingSessionScope : IDisposable
    {
        [ThreadStatic]
        private static ReportingSessionScope _current = null;

        public static ReportingSessionScope Current
        {
            get { return _current; }
        }

        private ISession _session = null;
        public static ISession CurrentSession
        {
            get { return ReportingSessionScope.Current._session; }
        }

        public ReportingSessionScope()
        {
            if (_current != null)
                throw new Exception("Session scope already active");
            //  _session = NHibernateManager.GetSessionFactory().OpenSession(new AuditInterceptor());
            _session = NHibernateManager.GetReportingSessionFactory().OpenSession();
            _current = this;
        }

        public ReportingSessionScope(FlushMode flushMode = FlushMode.Manual)
            : this()
        {
            _session.FlushMode = flushMode;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_session != null)
            {
                _session.Dispose();
                _session = null;
            }

            _current = null;
        }

        #endregion
    }

    public class DefaultSessionScope : IDisposable
    {
        [ThreadStatic]
        private SessionScope _myScope = null;

        public DefaultSessionScope()
        {
            if (SessionScope.Current == null)
                _myScope = new SessionScope();
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_myScope != null)
            {
                _myScope.Dispose();
                _myScope = null;
            }
        }

        #endregion
    }

    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override bool OnFlushDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, IType[] types)
        {
            //for (var i = 0; i < currentState.Length; i++)
            //{
            //    if (previousState[i] != currentState[i] && !previousState[i].Equals(currentState[i]))
            //    {
            //        var user = Globals.ContextCallBack.GetAppContext().User;
            //        var entityName = entity.GetType().ToString();
            //        Trace.WriteLine($"DIFF: ${propertyNames[i]}, OLD=${previousState[i]}, NEW=${currentState[i]}");
            //    }
                    
            //}

            return base.OnFlushDirty(entity, id, currentState, previousState, propertyNames, types);
        }

        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            if (sql.ToString().ToUpper().Contains("UPDATE "))
            {
                var ss = sql.ToString();
                //Trace.WriteLine(ss);
            }
            return sql;
        }

        public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            return base.OnSave(entity, id, state, propertyNames, types);
        }
    }

}
