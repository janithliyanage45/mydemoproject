﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using ERP.Core.Entities;
using NHibernate.Linq;

namespace ERP
{
    public class SettingAttribute : Attribute
    {
    }

    public class IsEvision : Attribute
    {
    }

    public class SysConfig
    {
        #region Accounting
        [Setting, Category("Accounting")]
        [DisplayName("Section Codes"), DefaultValue(new[] { "ADMIN", "PROD", "MKTG" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] SectionCodes { get; set; }

        [Setting, Category("Accounting")]
        [DisplayName("Enable Posting"), DefaultValue(false)]
        public bool EnablePosting { get; set; }


        [Setting, Category("Accounting")]
        [DisplayName("Auto JV Posting"), DefaultValue(true)]
        public bool AutoJVPosting { get; set; }

        [Setting, Category("Accounting")]
        [DisplayName("Default Stock Account No"), DefaultValue("12520")]
        public string DefaultStockAccountNo { get; set; }

        [Setting, Category("Accounting")]
        [DisplayName("Sales Categories"), DefaultValue(new[] { "None", "Sale", "Alignment", "Service" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] SalesCategories { get; set; }

        [Setting, Category("Accounting")]
        [DisplayName("Show All Invoices to Pay"), DefaultValue(false)]
        public bool ShowAllInvoicesToPay { get; set; }
        #endregion

        #region Common Settings
        [Setting, Category("Common Settings")]
        [DisplayName("Bank Names"), DefaultValue(new[] { "BOC", "Commercial", "DFCC" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] BankNames { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Thread Patterns"), DefaultValue(new[] { "A", "B", "C" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] ThreadPatterns { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Allow zero stock sales"), DefaultValue(false)]
        public bool AllowZeroStockSales { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Allow purchase from factory"), DefaultValue(true)]
        public bool AllowPurchaseFromFactory { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Popup Credit Info"), DefaultValue(false)]
        public bool PopUpCreditInfo { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Brandwise invoice serial numbers"), DefaultValue(false)]
        public bool BrandWiseInoiceSerials { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Superdag"), DefaultValue(false)]
        public bool IsSuperDag { get; set; }

        [Setting, Category("Common Settings")]
        [DisplayName("Base URL"), DefaultValue("")]
        public string BaseUrl { get; set; }

        #endregion

        #region Data Sync
        [Setting, Category("Data Sync")]
        [DisplayName("Master Accounts Connection Name"), DefaultValue("")]
        public string MasterAccountsConnName { get; set; }

        #endregion

        #region Chque Printing
        [Setting, Category("Chque Printing")]
        [DefaultValue(true)]
        public virtual bool UsePrintDriver { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(true)]
        public virtual bool ChequePrintFullYear { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeAccountPayeeX { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeAccountPayeeY { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeNonNegotiableX { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeNonNegotiableY { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequePaperFeedingIndex { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeLeftOffset { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeTopOffset { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeRightOffset { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(125)]
        public virtual int ChequeAmountLine1Y { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(150)]
        public virtual int ChequeAmountLine2Y { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(175)]
        public virtual int ChequeAmountLine3Y { get; set; }

        [Setting, Category("Chque Printing")]
        [DisplayName("Select AccountPayee"), DefaultValue(false)]
        public bool SelectAccountPayeeAndNonNegotiable { get; set; }

        [Setting, Category("Chque Printing")]
        [DisplayName("Select NonNegotiable"), DefaultValue(false)]
        public bool SelectNonNegotiable { get; set; }

        [Setting, Category("Chque Printing")]
        [DefaultValue(0)]
        public virtual int ChequeBottomOffset { get; set; }

        [Setting, Category("Chque Printing")]
        [DisplayName("Landscape"), DefaultValue(false)]
        public bool ChequeLandscape { get; set; }
        #endregion

        #region Commision
        [Setting, Category("Commission"), DefaultValue(75)]
        public virtual int CommissionBandLimit4 { get; set; }

        [Setting, Category("Commission"), DefaultValue(90)]
        public virtual int CommissionDeductionLimit { get; set; }
        #endregion

        #region SMS

        [Setting, Category("SMS")]
        [DisplayName("Gateway"), DefaultValue("")]
        public string SMSGateway { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("SMSUserName"), DefaultValue("")]
        public string SMSUserName { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("SMSPassword"), DefaultValue("")]
        public string SMSPassword { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("SMSSource"), DefaultValue("")]
        public string SMSSource { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("SMSGatewayIp"), DefaultValue("")]
        public string SMSGatewayIp { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("SMSGatewayPort"), DefaultValue("")]
        public string SMSGatewayPort { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("Footer"), DefaultValue("Test Company")]
        public string SMSFooter { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("Sending Interval"), DefaultValue("60")]
        public int SmsSendingInterval { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("Resend Failed Messages"), DefaultValue(false)]
        public bool ResendFailedMessages { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("Failed Message Retry Cont"), DefaultValue("5")]
        public int FailedMessageRetryCont { get; set; }

        [Setting, Category("SMS")]
        [DisplayName("Failed Message Retry Delay"), DefaultValue("5")]
        public int FailedMessageRetryDelay { get; set; }
        #endregion

        #region Elsa Workflow

        [Setting, Category("Elsa Workflow")]
        [DisplayName("IP"), DefaultValue("https://localhost")]
        public string ElsaWorkflowIP { get; set; }

        [Setting, Category("Elsa Workflow")]
        [DisplayName("Port"), DefaultValue("44332")]
        public string ElsaWorkflowPort { get; set; }
        #endregion

        #region Email
        [Setting, Category("E-mail")]
        [DisplayName("Host Name"), DefaultValue("smtp.gmail.com")]
        public string HostName { get; set; }

        [Setting, Category("E-mail")]
        [DisplayName("Port"), DefaultValue("587")]
        public int Port { get; set; }

        [Setting, Category("E-mail")]
        [DisplayName("Default Credentials"), DefaultValue(false)]
        public bool DefaultCredentials { get; set; }

        [Setting, Category("E-mail")]
        [DisplayName("Username"), DefaultValue("eVisionmicrosystems@gmail.com")]
        public string Username { get; set; }

        [Setting, Category("E-mail")]
        [DisplayName("Password"), DefaultValue("eVision123"), PasswordPropertyText(true)]
        public string Password { get; set; }

        [Setting, Category("E-mail")]
        [DisplayName("Sending Interval"), DefaultValue("60")]
        public int EmailSendingInterval { get; set; }

        #endregion

        #region RMA
        [Setting, Category("RMA")]
        [Browsable(false), DisplayName("Ready To Collect Status Id"), DefaultValue(1)]
        public int ReadyToCollectStatusId { get; set; }

        [Setting, Category("RMA")]
        [Browsable(false), DisplayName("Completed Status Id"), DefaultValue(1)]
        public int CompletedStatusId { get; set; }

        [Setting, Category("RMA")]
        [Browsable(false), DisplayName("Cancel Status Id"), DefaultValue(1)]
        public int RmaCancelStatusId { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Complete Job After Invoicing"), DefaultValue(false)]
        public bool CompleteJobAfterInvoicing { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Apply Job Cost To Job Invoice"), DefaultValue(false)]
        public bool ApplyJobCostToJobInvoice { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("RMA Return Location Id"), DefaultValue(0)]
        public int RMAReturnLocationId { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Allow Project Jobs"), DefaultValue(false)]
        public bool AllowProjectJobs { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("RMA Repaired Item GRN Location Id"), DefaultValue(0)]
        public int RMAGRNLocationId { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("RMA New Item GRN Location Id"), DefaultValue(0)]
        public int RMANewGRNLocationId { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Show Job History"), DefaultValue(false)]
        public bool ShowJobHistory { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("New Warranty Button Caption"), DefaultValue("New Warranty")]
        public string WarrantyButtonCaption { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("New Repair Button Caption"), DefaultValue("New Repair")]
        public string RepairButtonCaption { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Use Seperate Job Invoice"), DefaultValue(false)]
        public bool UseSeperateJobInvoice { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Allow To Change Job Type"), DefaultValue(false)]
        public bool AllowToChangeJobType { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Job Invoice Type Id"), DefaultValue(340)]
        public int JobInvoiceTypeId { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Enable Customer Editing"), DefaultValue(false)]
        public bool JobCardEnableCustomerEditing { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 1 Data"), DefaultValue("")]
        public string JobCardCustomList1Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 2 Data"), DefaultValue("")]
        public string JobCardCustomList2Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 3 Data"), DefaultValue("")]
        public string JobCardCustomList3Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 4 Data"), DefaultValue("")]
        public string JobCardCustomList4Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 5 Data"), DefaultValue("")]
        public string JobCardCustomList5Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 6 Data"), DefaultValue("")]
        public string JobCardCustomList6Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 7 Data"), DefaultValue("")]
        public string JobCardCustomList7Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 8 Data"), DefaultValue("")]
        public string JobCardCustomList8Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Custom List 9 Data"), DefaultValue("")]
        public string JobCardCustomList9Data { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("RMA Default Customer Id"), DefaultValue(0)]
        public int RMADefaultCustomerId { get; set; }

        [Setting, Category("RMA")]
        [DisplayName("Allow Auto Load Product By SerialNo"), DefaultValue(false)]
        public bool AllowAutoLoadProductBySerialNo { get; set; }

        #endregion

        #region Option

        [Setting, Category("Option")]
        [DisplayName("Basic System"), DefaultValue(false)]
        public bool IsBasicSystem { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Enable Free Issue Scheme"), DefaultValue(false)]
        public bool EnableFreeIssueScheme { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Enable Minus Quantity"), DefaultValue(false)]
        public bool EnableMinusQuantity { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Validate Product Min Price"), DefaultValue(false)]
        public bool ValidateProductMinPrice { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Separate Tax Invoice Serial "), DefaultValue(false)]
        public bool SeparateTaxInvoiceSerial { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Hide legal action Customers in Outstanding Report"), DefaultValue(false)]
        public bool HideLegalActionCustomersInOutstandingReport { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Enable Invoice Direct Printing"), DefaultValue(false)]
        public bool EnableInvoiceDirectPrinting { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Enable Auto Discount"), DefaultValue(false)]
        public bool EnableAutoDiscount { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Enable Auto Return"), DefaultValue(false)]
        public bool EnableAutoReturn { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Enable Aproved GRN Payment"), DefaultValue(false)]
        public bool EnableAprovedGRNPayment { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Allow To Return Part Issue Items"), DefaultValue(false)]
        public bool AllowReturnPartIssueItems { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Value Decimal Point"), DefaultValue(2)]
        public int ValueDecimalPoint { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Calculate Average Cost For Uploaded Products Only"), DefaultValue(false)]
        public bool CalculateAverageCostForUploadedProductsOnly { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Total Decimal Point"), DefaultValue(2)]
        public int TotalDecimalPoint { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Crdit Request Emails"), DefaultValue(new[] { "yuthik@evisionmicro.com", "janith@evisionmicro.com", "eranga@evisionmicro.com" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] CrditRequestEmails { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Admin Phone Numbers"), DefaultValue(new[] { "" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] AdminPhoneNumbers { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Update Product Min Qty When Cal Reorder Point"), DefaultValue(false)]
        public bool UpdateProductMinQty { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Period For Reorder Stock Calculation"), DefaultValue("3")]
        public int PeriodForReorderStockCalculation { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Buffer Time For Stock Calculation"), DefaultValue("0")]
        public int BufferTimeForStockCalculation { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Show Stock Location wise product stock in Product window"), DefaultValue(false)]
        public bool ShowStockLocationWiseProductStockInProductWindow { get; set; }

        [Setting, Category("Company")]
        [DisplayName("DB Backup Server Url"), DefaultValue("http://localhost:9999")]
        public string DbBackupServerUrl { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Manual Refresh Dashboard"), DefaultValue(false)]
        public bool ManualRefreshDashboard { get; set; }

        #endregion

        #region Campany

        [Setting, Category("Company")]
        [DisplayName("Comapany Group Name"), DefaultValue("Company Group")]
        public string CompanyGroupName { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Name"), DefaultValue("-")]
        public string CompanyName { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Address"), DefaultValue("-")]
        public string CompanyAddress { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Show Logo"), DefaultValue(true)]
        public bool ShowLogo { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Branch name"), DefaultValue("-")]
        public string BranchName { get; set; }

        [Setting, Category("Company")]
        [DisplayName("VAT Reg No"), DefaultValue("-")]
        public string VATRegNo { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Cash Sale Account Number"), DefaultValue("")]
        public string CashSalesAccountNo { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Start Date"), DefaultValue("01/01/2017")]
        public DateTime StartDate { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Last Customer Id"), DefaultValue(0)]
        public int LastCustomerId { get; set; }

        [Setting, Category("Company")]
        [DisplayName("No Of Week End Days"), DefaultValue(1)]
        public int NoOfWeekEndDays { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Brand Name"), DefaultValue("SFA System")]
        public string BrandName { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Page Title"), DefaultValue("SFA System")]
        public string PageTitle { get; set; }

        [Setting, Category("Company")]
        [DisplayName("Footer Text"), DefaultValue("eVision Microsystems (Pvt) Ltd © 2015-2020")]
        public string FooterText { get; set; }

        [Setting, Category("Option")]
        [DisplayName("Admin Emails"), DefaultValue(new[] { "" })]
        [TypeConverter(typeof(StringArryConverter))]
        public string[] AdminEmails { get; set; }
        #endregion

        #region Field Captions

        [Setting, Category("Field Captions"), DefaultValue("Ply Rate")]
        public virtual string ClassficationCaption { get; set; }

        [Setting, Category("Field Captions"), DefaultValue("Pattern")]
        public virtual string ModelCaption { get; set; }

        [Setting, Category("Field Captions"), DefaultValue("Size")]
        public virtual string SizeCaption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Product Custom Field 1 Caption"), DefaultValue("Other Info 1")]
        public string ProductCustomField1Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Product Custom Field 2 Caption"), DefaultValue("Other Info 2")]
        public string ProductCustomField2Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Product Custom Field 3 Caption"), DefaultValue("Other Info 3")]
        public string ProductCustomField3Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Product Custom Field 4 Caption"), DefaultValue("Other Info 4")]
        public string ProductCustomField4Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 1 Caption"), DefaultValue("Custom 1")]
        public string JobCardCustomField1Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 2 Caption"), DefaultValue("Custom 2")]
        public string JobCardCustomField2Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 3 Caption"), DefaultValue("Custom 3")]
        public string JobCardCustomField3Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 4 Caption"), DefaultValue("Custom 4")]
        public string JobCardCustomField4Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 5 Caption"), DefaultValue("Custom 5")]
        public string JobCardCustomField5Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 6 Caption"), DefaultValue("Custom 6")]
        public string JobCardCustomField6Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 7 Caption"), DefaultValue("Custom 7")]
        public string JobCardCustomField7Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 8 Caption"), DefaultValue("Custom 8")]
        public string JobCardCustomField8Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 9 Caption"), DefaultValue("Custom 9")]
        public string JobCardCustomField9Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom Field 10 Caption"), DefaultValue("Custom 10")]
        public string JobCardCustomField10Caption { get; set; }


        [Setting, Category("Field Captions")]
        [DisplayName("Invoice Custom Field 2 Caption"), DefaultValue("Custom 2")]
        public string InvoiceCustomField2Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Invoice Custom Field 3 Caption"), DefaultValue("Custom 3")]
        public string InvoiceCustomField3Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Invoice Custom Field 4 Caption"), DefaultValue("Custom 4")]
        public string InvoiceCustomField4Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Invoice Custom Field 5 Caption"), DefaultValue("Custom 5")]
        public string InvoiceCustomField5Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Invoice Custom Field 6 Caption"), DefaultValue("Custom 6")]
        public string InvoiceCustomField6Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 1 Caption"), DefaultValue("Custom List 1")]
        public string JobCardCustomList1Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 2 Caption"), DefaultValue("Custom List 2")]
        public string JobCardCustomList2Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 3 Caption"), DefaultValue("Custom List 3")]
        public string JobCardCustomList3Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 4 Caption"), DefaultValue("Custom List 4")]
        public string JobCardCustomList4Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 5 Caption"), DefaultValue("Custom List 5")]
        public string JobCardCustomList5Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 6 Caption"), DefaultValue("Custom List 6")]
        public string JobCardCustomList6Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 7 Caption"), DefaultValue("Custom List 7")]
        public string JobCardCustomList7Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 8 Caption"), DefaultValue("Custom List 8")]
        public string JobCardCustomList8Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Job Card Custom List 9 Caption"), DefaultValue("Custom List 9")]
        public string JobCardCustomList9Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Show External Party Custom Field 1"), DefaultValue(false)]
        public bool ShowCustomerCustomField1 { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Show External Party Custom Field 2"), DefaultValue(false)]
        public bool ShowCustomerCustomField2 { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Show External Party Custom Field 3"), DefaultValue(false)]
        public bool ShowCustomerCustomField3 { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Show External Party Custom Field 4"), DefaultValue(false)]
        public bool ShowCustomerCustomField4 { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Show External Party Custom Field 5"), DefaultValue(false)]
        public bool ShowCustomerCustomField5 { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("Show External Party Custom Field 6"), DefaultValue(false)]
        public bool ShowCustomerCustomField6 { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("External Party Custom Field 1 Caption"), DefaultValue("Custom Field 1")]
        public string ExternalPartyCustomField1Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("External Party Custom Field 2 Caption"), DefaultValue("Custom Field 2")]
        public string ExternalPartyCustomField2Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("External Party Custom Field 3 Caption"), DefaultValue("Custom Field 3")]
        public string ExternalPartyCustomField3Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("External Party Custom Field 4 Caption"), DefaultValue("Custom Field 4")]
        public string ExternalPartyCustomField4Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("External Party Custom Field 5 Caption"), DefaultValue("Custom Field 5")]
        public string ExternalPartyCustomField5Caption { get; set; }

        [Setting, Category("Field Captions")]
        [DisplayName("External Party Custom Field 6 Caption"), DefaultValue("Custom Field 5")]
        public string ExternalPartyCustomField6Caption { get; set; }

        #endregion

        #region Job Card
        [Setting, Category("Job Card")]
        [DisplayName("Show Quoted Amount"), DefaultValue(false)]
        public bool ShowJobQuotedAmount { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Description"), DefaultValue(false)]
        public bool ShowJobDescription { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Fault"), DefaultValue(false)]
        public bool ShowJobFault { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Resolution"), DefaultValue(false)]
        public bool ShowJobResolution { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Customer"), DefaultValue(false)]
        public bool ShowJobCustomer { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Supplier"), DefaultValue(false)]
        public bool ShowJobSupplier { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Serial"), DefaultValue(false)]
        public bool ShowJobSerial { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Product"), DefaultValue(false)]
        public bool ShowJobProduct { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Product Code"), DefaultValue(false)]
        public bool ShowJobProductCode { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Rep"), DefaultValue(false)]
        public bool ShowJobRep { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Ref No"), DefaultValue(false)]
        public bool ShowJobRefNo { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Customer Phone"), DefaultValue(false)]
        public bool ShowJobCustomerPhone { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Customer Mobile"), DefaultValue(false)]
        public bool ShowJobCustomerMobile { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 1"), DefaultValue(false)]
        public bool ShowJobCustomField1 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 2"), DefaultValue(false)]
        public bool ShowJobCustomField2 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 3"), DefaultValue(false)]
        public bool ShowJobCustomField3 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 4"), DefaultValue(false)]
        public bool ShowJobCustomField4 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 5"), DefaultValue(false)]
        public bool ShowJobCustomField5 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 6"), DefaultValue(false)]
        public bool ShowJobCustomField6 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 7"), DefaultValue(false)]
        public bool ShowJobCustomField7 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 8"), DefaultValue(false)]
        public bool ShowJobCustomField8 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 9"), DefaultValue(false)]
        public bool ShowJobCustomField9 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom Field 10"), DefaultValue(false)]
        public bool ShowJobCustomField10 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 7 "), DefaultValue(false)]
        public bool ShowJobCustomList1 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 2 "), DefaultValue(false)]
        public bool ShowJobCustomList2 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 3 "), DefaultValue(false)]
        public bool ShowJobCustomList3 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 4 "), DefaultValue(false)]
        public bool ShowJobCustomList4 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 5 "), DefaultValue(false)]
        public bool ShowJobCustomList5 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 6 "), DefaultValue(false)]
        public bool ShowJobCustomList6 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 7 "), DefaultValue(false)]
        public bool ShowJobCustomList7 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 8 "), DefaultValue(false)]
        public bool ShowJobCustomList8 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Show Custom List 9 "), DefaultValue(false)]
        public bool ShowJobCustomList9 { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Hide Other Status Info "), DefaultValue(false)]
        public bool HideJobOtherStatus { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Hide Brand Details"), DefaultValue(false)]
        public bool HideJobBrandDetails { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Hide Supplier Details"), DefaultValue(false)]
        public bool HideJobSupplierDetails { get; set; }

        [Setting, Category("Job Card")]
        [DisplayName("Enable New Job Card"), DefaultValue(false)]
        public bool IsNewJobCard { get; set; }
        #endregion

        #region Payment
        [Setting, Category("Payment")]
        [DisplayName("Print Only Approved Payment"), DefaultValue(false)]
        public bool AllowedPrintOnlyApprovedPayment { get; set; }

        [Setting, Category("Payment")]
        [DisplayName("Validate Approved Payment"), DefaultValue(false)]
        public bool ValidateApprovedPayments { get; set; }

        [Setting, Category("Payment")]
        [DisplayName("Enable Back Date"), DefaultValue(false)]
        public bool EnablePaymentBackDate { get; set; }

        [Setting, Category("Payment")]
        [DisplayName("Show All Cheques To Deposit"), DefaultValue(false)]
        public bool ShowAllChequesToDeposit { get; set; }

        [Setting, Category("Payment")]
        [DisplayName("Only Show Branch Cashbooks"), DefaultValue(false)]
        public bool ShowBranchCashbooksOnly { get; set; }

        [Setting, Category("Payment")]
        [DisplayName("Only Show Branch Cashbooks"), DefaultValue(false)]
        public bool LoadAllCashBookAccounts { get; set; }



        #endregion

        #region SFA
        [Setting, Category("SFA")]
        [DisplayName("Route wise document serial for PDA"), DefaultValue(false)]
        public bool RouteWiseSerial { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Enable Auto Send Credit Request For PDA SO "), DefaultValue(false)]
        public bool EnableAutoSendCreditRequestForPDASO { get; set; }


        [Setting, Category("SFA")]
        [DisplayName("Show with out reserving Qty "), DefaultValue(false)]
        public bool ShowWithOutReservingQty { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("KPI Download avoid in Peek Time"), DefaultValue(false)]
        public bool KPIDownloadAvoidInPeekTime { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Show Target with Traget Category wise"), DefaultValue(false)]
        public bool ShowTargetwithTragetCategorywise { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Product Download Validate"), DefaultValue(false)]
        public bool ProductDownloadValidate { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Download All Stock Product Qty"), DefaultValue(false)]
        public bool DownloadAllStockProductQty { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Validate Invoice"), DefaultValue(false)]
        public bool ValidateInvoice { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Track Reps"), DefaultValue(false)]
        public bool TrackReps { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Min Distance Change For Update"), DefaultValue(0)]
        public decimal MinDistanceChangeForUpdate { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Min Time BW Update"), DefaultValue(0)]
        public decimal MinTimeBWUpdate { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Is SFA"), DefaultValue(false)]
        public bool IsSFA { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Duty On Time"), DefaultValue("")]
        public string DutyOnTime { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Duty Off Time"), DefaultValue("")]
        public string DutyOffTime { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Disable Self Host"), DefaultValue(false)]
        public bool DisableSelfHost { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Enable Auto Posting in Self Host"), DefaultValue(true)]
        public bool EnablePostingInSelfHost { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Loading sheet without products loading"), DefaultValue(false)]
        public bool LoadingSheetWithoutProductsLoading { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Image Analyzer API Key"), DefaultValue("")]
        public string ImageAnalyzerAPIKey { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("PDA Version"), DefaultValue("5.0")]
        public string PDAVersion { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Rep Level Id"), DefaultValue("0")]
        public int RepLevelId { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Location Descrepancy Threshold"), DefaultValue("0")]
        public int LocationDescrepancyThreshold { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Extend Territory"), DefaultValue(false)]
        public bool IsTerritoryExtended { get; set; }


        [Setting, Category("SFA")]
        [DisplayName("Enable Priority base Discount Schemes"), DefaultValue(false)]
        public bool ISPriorityBaseDiscountSchemesEnable { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Enable Call Lost Process"), DefaultValue(false)]
        public bool EnableCallLostProcess { get; set; }

        [Setting, Category("SFA")]
        [DisplayName("Use Value Achievement"), DefaultValue(false)]
        public bool UseValueAchievement { get; set; }

        #endregion

        #region Stock
        [Setting, Category("Stock")]
        [Browsable(false), DisplayName("NSF Top Margin"), DefaultValue(0)]
        public int NSFTopMargin { get; set; }

        [Setting, Category("Stock")]
        [Browsable(false), DisplayName("NSF Bottom Margin"), DefaultValue(0)]
        public int NSFBottomMargin { get; set; }
        #endregion

        #region Rates
        [Setting, Category("Rates")]
        [DisplayName("Driver Rate"), DefaultValue(0)]
        public decimal DriverRate { get; set; }

        [Setting, Category("Rates")]
        [DisplayName("Helper 1 Rate"), DefaultValue(0)]
        public decimal Helper1Rate { get; set; }

        [Setting, Category("Rates")]
        [DisplayName("Helper 2 Rate"), DefaultValue(0)]
        public decimal Helper2Rate { get; set; }

        [Setting, Category("Rates")]
        [DisplayName("Entered User Rate"), DefaultValue(0)]
        public decimal EnteredUserRate { get; set; }

        [Setting, Category("Rates")]
        [DisplayName("Prepaired User Rate"), DefaultValue(0)]
        public decimal PrepairedUserRate { get; set; }

        [Setting, Category("Rates")]
        [DisplayName("Entered User Rate For Return"), DefaultValue(0)]
        public decimal EnteredUserRateReturn { get; set; }

        [Setting, Category("Rates")]
        [DisplayName("Store Keeper Rate"), DefaultValue(0)]
        public decimal StoreKeeperRate { get; set; }
        #endregion

        #region Other
        [Setting, Category("Other")]
        [DisplayName("Custom Setting 1"), DefaultValue("")]
        public string CustomSetting1 { get; set; }

        [Setting, Category("Other")]
        [DisplayName("Custom Setting 2"), DefaultValue("")]
        public string CustomSetting2 { get; set; }

        [Setting, Category("Other")]
        [DisplayName("Custom Setting 3"), DefaultValue("")]
        public string CustomSetting3 { get; set; }

        [Setting, Category("Other")]
        [DisplayName("Custom Setting 4"), DefaultValue("")]
        public string CustomSetting4 { get; set; }
        #endregion

        #region Invoice
        [Setting, Category("Invoice")]
        [DisplayName("Enable BackDate"), DefaultValue(false)]
        public bool EnableBackDate { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Allow Duplicate Serials"), DefaultValue(false)]
        public bool AllowDuplicateSerials { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Auto Call Discount"), DefaultValue(false)]
        public bool AutoCallDiscount { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Required Select Parent Invoice"), DefaultValue(false)]
        public bool RequiredSelectParentInvoice { get; set; }


        [Setting, Category("Invoice")]
        [DisplayName("Enable Edit"), DefaultValue(false)]
        public bool EnableInvoiceEdit { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("New PO Window"), DefaultValue(false)]
        public bool NewPO { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Call Quota For Value"), DefaultValue(false)]
        public bool CallQuotaForValue { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Validate Credit Amount"), DefaultValue(false)]
        public bool ValidateCreditAmount { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Validate Credit Days"), DefaultValue(false)]
        public bool ValidateCreditDays { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Enable Decimal Qty"), DefaultValue(false)]
        public bool EnableDecimalQty { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Use PrintDrver"), DefaultValue(true)]
        public virtual bool UsePrintDriverInvoice { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Auto Apply Vat Line"), DefaultValue(false)]
        public virtual bool AutoApplyVatLine { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Show Custom Field 2"), DefaultValue(false)]
        public bool ShowInvoiceCustomField2 { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Show Custom Field 3"), DefaultValue(false)]
        public bool ShowInvoiceCustomField3 { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Show Custom Field 4"), DefaultValue(false)]
        public bool ShowInvoiceCustomField4 { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Show Custom Field 5"), DefaultValue(false)]
        public bool ShowInvoiceCustomField5 { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Show Custom Field 6"), DefaultValue(false)]
        public bool ShowInvoiceCustomField6 { get; set; }


        [Setting, Category("Invoice")]
        [DisplayName("Validate Discount"), DefaultValue(false)]
        public bool ValidateDiscount { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Validate Mass Discount"), DefaultValue(false)]
        public bool ValidateMassDiscount { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Max Manager Mass Discount Ptc"), DefaultValue(0)]
        public decimal MaxManagerMassDiscountPct { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Max Rep Mass Discount Ptc"), DefaultValue(0)]
        public decimal MaxRepMassDiscountPct { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Invoice Requred For Delivery Order"), DefaultValue(true)]
        public bool IsInvoiceRequredForDeliveryOrder { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Show Other Party Address"), DefaultValue(false)]
        public bool ShowOtherPartyAddress { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("PO Company Stock Wise Color Change"), DefaultValue(false)]
        public bool POCompanyStockWiseColorChange { get; set; }

        [Setting, Category("Invoice")]
        [DisplayName("Convert Date to Doc Date"), DefaultValue(false)]
        public bool ConvertDocDateIsConvertDate { get; set; }

        #endregion

        #region SFA Sync

        [Setting, Category("SFA Sync")]
        [DisplayName("ERP Base URL"), DefaultValue("")]
        public string ERPBaseURL { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Max Row Count"), DefaultValue(10)]
        public int MaxRowCount { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Enable Sync"), DefaultValue(false)]
        public bool EnableSync { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Enable PO Sync"), DefaultValue(false)]
        public bool SyncPo { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Enable Master Data Sync"), DefaultValue(false)]
        public bool SyncMasterData { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Sync Interval (Seconds)"), DefaultValue(300)]
        public int SyncInterval { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("PO Sync Interval (Seconds)"), DefaultValue(900)]
        public int POSyncInterval { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Master Data Sync Interval (Seconds)"), DefaultValue(3600)]
        public int MasterDataSyncInterval { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Invoice Download Period (Days)"), DefaultValue(1)]
        public int InvoiceDownloadPeriod { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Cancelled Invoice Download Period (Days)"), DefaultValue(1)]
        public int CancelledInvoiceDownloadPeriod { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Rejected PO Resync Interval (Seconds)"), DefaultValue(900)]
        public int RejectedPOResyncInterval { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Enable Company Stock Sync"), DefaultValue(false)]
        public bool SyncCompanyStock { get; set; }

        [Setting, Category("SFA Sync")]
        [DisplayName("Company Stock Sync Time"), DefaultValue("0 30 2 * * ?")]
        public string CompanyStockSyncTime { get; set; }



        #endregion

        #region Shopping Cart

        [Setting, Category("Shopping Cart")]
        [DisplayName("Default Customer Code"), DefaultValue("")]
        public string DefaultShoppingCartCustomerCode { get; set; }

        #endregion

        #region eVision

        [Setting, Category("eVision")]
        [DisplayName("ShowHpMenu"), DefaultValue(true), IsEvision]
        public bool ShowHpMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("ShowSpecialTypeMenu"), DefaultValue(true), IsEvision]
        public bool ShowSpecialTypeMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("ShowTransportMenu"), DefaultValue(true), IsEvision]
        public bool ShowTransportMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("ShowProductionMenu"), DefaultValue(true), IsEvision]
        public bool ShowProductionMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("ShowSFAMenu"), DefaultValue(true), IsEvision]
        public bool ShowSFAMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("ShowAccountsMenu"), DefaultValue(true), IsEvision]
        public bool ShowAccountsMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("ShowRepairCenterMenu"), DefaultValue(true), IsEvision]
        public bool ShowRepairCenterMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("Show SFA Menu"), DefaultValue(true), IsEvision]
        public bool ShowFAMenu { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("Enable CRM"), DefaultValue(false), IsEvision]
        public bool EnableCRM { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("Enable Incentive Module"), DefaultValue(false), IsEvision]
        public bool EnableIncentiveModule { get; set; }


        [Setting, Category("eVision")]
        [DisplayName("IsNewInvoice"), DefaultValue(true), IsEvision]
        public bool IsNewInvoice { get; set; }

        [Setting, Category("eVision")]
        [DisplayName("IsNewMenu"), DefaultValue(false), IsEvision]
        public bool IsNewMenu { get; set; }


        #endregion

        #region Serial Prefixes

        [Setting, Category("Serial Prefixes")]
        [DisplayName("Route Serial Prefix"), DefaultValue("RT")]
        public string RouteSerialPrefix { get; set; }

        [Setting, Category("Serial Prefixes")]
        [DisplayName("Rep Serial Prefix"), DefaultValue("REP")]
        public string RepSerialPrefix { get; set; }

        #endregion

        #region Configurations

        [Setting, Category("Configurations")]
        [DisplayName("Enable Auto Request Credit Approvals"), DefaultValue(false)]
        public bool EnableAutoRequestCreditApprovals { get; set; }

        [Setting, Category("Configurations")]
        [DisplayName("Enable Backdating"), DefaultValue(false)]
        public bool EnableBackDating { get; set; }

        [Setting, Category("Configurations")]
        [DisplayName("Enable Azure AD Sign In"), DefaultValue(false)]
        public bool EnableAzureADSignIn { get; set; }

        #endregion


        public static SysConfig LoadConfig()
        {
            using (new DefaultSessionScope())
            {
                var cfg = new SysConfig();
                cfg.Init();
                return cfg;
            }
        }

        private void Init()
        {
            PropertyInfo[] props = typeof(SysConfig).GetProperties();
            foreach (PropertyInfo p in props)
            {
                if (!p.IsDefined(typeof(SettingAttribute), false))
                    continue;

                var param = SysParam.GetParam(SessionScope.CurrentSession, p.Name);
                if (param == null)
                {
                    param = new SysParam();
                    param.Name = p.Name;
                    param.ValueType = SysParam.ToSysParamType(p.PropertyType);

                    var defValAttr = p.GetCustomAttributes(typeof(DefaultValueAttribute), false).FirstOrDefault() as DefaultValueAttribute;
                    param.Value = ToStringValue(defValAttr.Value);
                    SessionScope.CurrentSession.Save(param);
                }

                if (p.PropertyType == typeof(DateTime))
                {
                    p.SetValue(this, param.DateValue, null);
                }
                else
                {
                    p.SetValue(this, param.ObjectValue, null);
                }
            }
        }

        public void SaveChanges()
        {
            using (var txn = SessionScope.CurrentSession.BeginTransaction())
            {
                PropertyInfo[] props = typeof(SysConfig).GetProperties();
                foreach (PropertyInfo p in props)
                {
                    if (!p.IsDefined(typeof(SettingAttribute), false))
                        continue;

                    var param = SysParam.GetParam(SessionScope.CurrentSession, p.Name);
                    var value = p.GetValue(this, null);
                    param.Value = ToStringValue(value);
                    SessionScope.CurrentSession.Update(param);
                }

                txn.Commit();
            }
        }

        public static string ToStringValue(object value)
        {
            if (value == null)
                return "";

            if (!value.GetType().IsArray)
                return value.ToString();

            var valueArray = (object[])value;
            var result = "";
            foreach (var x in valueArray)
                result += x + "|";

            return result.Trim('|');
        }

        public static string FindValueByName(string name)
        {
            using (new DefaultSessionScope())
            {
                var setting = SessionScope.CurrentSession.Query<SysParam>().FirstOrDefault(x => x.Name == name);
                return setting != null ? setting.Value : null;
            }
        }
    }

    public class StringArryConverter : TypeConverter
    {
        // Overrides the CanConvertFrom method of TypeConverter.
        // The ITypeDescriptorContext interface provides the context for the
        // conversion. Typically, this interface is used at design time to 
        // provide information about the design-time container.
        public override bool CanConvertFrom(ITypeDescriptorContext context,
           Type sourceType)
        {

            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }
        // Overrides the ConvertFrom method of TypeConverter.
        public override object ConvertFrom(ITypeDescriptorContext context,
           CultureInfo culture, object value)
        {
            if (value is string)
            {
                return ((string)value).Split('|');
            }
            return base.ConvertFrom(context, culture, value);
        }
        // Overrides the ConvertTo method of TypeConverter.
        public override object ConvertTo(ITypeDescriptorContext context,
           CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (value == null)
                    return null;

                var lst = (string[])value;
                var result = "";
                foreach (var x in lst)
                    result += x + "|";
                return result.Trim('|');
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }


    }

}
