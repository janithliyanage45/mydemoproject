using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ERP
{
    public class SeP
    {
        public static string DSN { get; set; } //Default system name

        public static string[] GetAvailableSystemNames()
        {
            string x = "Sys", y = "temN", z = "ames";
            return System.Configuration.ConfigurationManager.AppSettings.Get(x + y + z)
                .Split('|');            
        }

        public static string[] GetAvailableBranchCodes(string systemName)
        {
            return System.Configuration.ConfigurationManager.AppSettings.Get(systemName + "_BRANCHES")
                .Split('|');
        }

        public static string GetX()
        {
            return GetX(DSN);
        }

        public static string GetX(string sn)
        {
            return QQ(System.Configuration.ConfigurationManager.AppSettings.Get(sn));
        }

        public static XYZ GetY()
        {
            return GetY(DSN);
        }

        public static XYZ GetY(string sn)
        {
            return new XYZ(GetX(sn));
        }

        public static Version GetAssemblyVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        public static string GetEndPointName()
        {
            return "BasicHttpBinding_IReportingService_" + DSN;
        }

        private static string QQ(string x)
        {
            var passIndex = x.IndexOf("rd=*", StringComparison.CurrentCultureIgnoreCase);
            if (passIndex < 0)
                return x;

            var semiInxex = x.IndexOf(';', passIndex + 1);
            var pass = x.Substring(passIndex + 4, semiInxex - passIndex - 4);
            try
            {
                var rr = new DocMan().BB(pass).Trim('\0');
                return x.Replace("*" + pass, rr);
            }
            catch (Exception ex)
            {
                return x;
            }
        }
    }

    public class XYZ
    {
        public string S { get; set; } //Server
        public string U { get; set; } //Username
        public string P { get; set; } //Password
        public string D { get; set; } //Database
        public string C { get; set; } //ConnStr

        public XYZ(string c)
        {
            C = c;

            string[] toks = c.Trim().Split(';');
            foreach (string tok in toks)
            {
                string[] arr = tok.Trim().Split('=');
                switch (char.ToUpper(arr[0][0]))
                {
                    case 'S':
                        if(S == null)
                        S = arr[1];
                        break;

                    case 'U':
                        if (U == null)
                        U = arr[1];
                        break;

                    case 'P':
                        if (P == null)
                        P = arr[1];
                        break;

                    case 'D':
                        if (D == null)
                        D = arr[1];
                        break;
                }
            }
        }
    }
}
