﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ERP
{
    public class DocMan
    {

        #region Members

        byte[] key;
        byte[] iv;
        SymmetricAlgorithm algorithm;

        byte[] Key
        {
            get { return key; }
            set { key = value; }
        }

        SymmetricAlgorithm Algorithm
        {
            get { return algorithm; }
            set { algorithm = value; }
        }
        #endregion

        public byte[] A(string str)
        {
            if (algorithm == null)
                algorithm = new RijndaelManaged();

            algorithm.Key = new ASCIIEncoding().GetBytes("1234567890@ABCDE");
            algorithm.IV = new byte[] { 150, 61, 167, 172, 4, 70, 54, 147, 65, 112, 139, 206, 1, 68, 244, 138 };

            iv = algorithm.IV;
            key = algorithm.Key;

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms,
                      algorithm.CreateEncryptor(),
                      CryptoStreamMode.Write);

            ASCIIEncoding ae = new ASCIIEncoding();
            byte[] plainBytes = ae.GetBytes(str);

            cs.Write(plainBytes, 0, plainBytes.Length);
            cs.FlushFinalBlock();

            return ms.ToArray();
        }

        public string AA(string str)
        {
            return HexEncoding.ToString(A(str));
        }

        public string B(byte[] str)
        {
            if (algorithm == null)
                algorithm = new RijndaelManaged();

            algorithm.Key = new ASCIIEncoding().GetBytes("1234567890@ABCDE");
            algorithm.IV = new byte[] { 150, 61, 167, 172, 4, 70, 54, 147, 65, 112, 139, 206, 1, 68, 244, 138 };

            MemoryStream ms = new MemoryStream(str);
            CryptoStream cs = new CryptoStream(ms,
                          algorithm.CreateDecryptor(),
                          CryptoStreamMode.Read);
            //algorithm.IV = iv;
            byte[] plainBytes = new byte[str.Length];
            cs.Read(plainBytes, 0, str.Length);

            ASCIIEncoding ae = new ASCIIEncoding();
            return ae.GetString(plainBytes);
        }

        public string BB(string str)
        {
            return B(HexEncoding.GetBytes(str));
        }

    }
}
