﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using ERP.Core.Entities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.Core
{
    public static class Utils
    {
       // private static IHttpContextAccessor _httpContextAccessor;
      

        public static string GetExceptionText(Exception ex)
        {
            //if (ex is EntityValidationException)
            //{
            //    return ex.ToString();
            //}

            if (ex.InnerException == null)
                return ex.Message;

            return ex.Message + "\r\n\r\n+ " + GetExceptionName(ex.InnerException) + GetExceptionText(ex.InnerException);
        }

        private static string GetExceptionName(Exception ex)
        {
            if (ex == null)
                return "";

            return ex.GetType().Name + "\r\n ";
        }

        public static string GetMd5Hash(string input)
        {
            using (var md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash. 
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes 
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data  
                // and format each one as a hexadecimal string. 
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("X2"));
                }

                // Return the hexadecimal string. 
                return sBuilder.ToString();
            }
        }

        public static string GetEncodedPassword(string password)
        {
            return GetMd5Hash(password + "@OKTAN");
        }

        public static string LastChar(this string s)
        {
            try
            {
                return s[s.Length - 1].ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }
        public static string GetEncriptedString(string text)
        {
            return ECrypto.Encrypt(text, "NaN0Ta*1");
        }
        public static string GetDecryptedString(string text)
        {
            return ECrypto.Decrypt(text, "NaN0Ta*1");
        }
        public static void WriteAuditTrail(int userId, string username, string category, string action,
            string message = "")
        {
            //var entry = new AuditTrailEntry()
            //{
            //    Date = DateTime.Now,
            //    UserId = userId,
            //    Username = username,
            //    Category = category,
            //    Action = action,
            //   // Message = message + " [" + HttpContext.Current.Request.UserHostAddress + "]"
            //};
            //TODO: Add Message

            //DAO<AuditTrailEntry>.Save(entry);
        }

        public static class EnumHelper<T>
        {
            public static string GetEnumDescription(string value)
            {
                Type type = typeof(T);
                var name = Enum.GetNames(type).Where(f => f.Equals(value, StringComparison.CurrentCultureIgnoreCase)).Select(d => d).FirstOrDefault();

                if (name == null)
                {
                    return string.Empty;
                }
                var field = type.GetField(name);
                var customAttribute = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                return customAttribute.Length > 0 ? ((DescriptionAttribute)customAttribute[0]).Description : name;
            }
        }

        public static string GetEnumDescriptions(Enum value)
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            var field = type.GetField(name);
            var fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true).ToList();

            return (fds.FirstOrDefault() as DescriptionAttribute).Description;

        }

        public static string GetConnectionString()
        {
            return ConfigurationManager.AppSettings[SeP.DSN];
        }

        public static string GetDbName()
        {
            var connectionString = Globals.AppSettings.GetSection("AppSettings").GetValue<string>("ConnStr");
            var settings = connectionString.Split(';').ToList();
            var dbNameString = settings.FirstOrDefault(x => x.Contains("database"));
            var data = dbNameString.Split('=');
            var dbName = data[1];
            return dbName;
        }

        public static string ToSeperateWords(this string text)
        {
            return Regex.Replace(text, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
        }

        public static List<SelectListItem> GetSelectItemList(List<string> stringList)
        {
            var selectItemList = new List<SelectListItem>();
            foreach (var val in stringList)
            {
                var item = new SelectListItem() { Value = val, Text = val, Selected = false };
                selectItemList.Add(item);
            }
            return selectItemList;
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = e, Name = e.ToString() };
            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static IEnumerable<SelectListItem> GetEnumSelectList<T>()
        {
            return (Enum.GetValues(typeof(T)).Cast<T>().Select(
                enu => new SelectListItem() { Text = enu.ToString(), Value = enu.ToString() })).ToList();
        }

        public static SelectList ToSelectList(this Type enumType)
        {
            if (enumType.IsEnum)
            {
                var list = Enum.GetValues(enumType)
                    .Cast<Enum>()
                    .Select(e => new SelectListItem()
                    {
                        Value = e.ToString(),
                        Text = e.ToString()
                    })
                    .ToList();

                return new SelectList(list, "Value", "Text");
            }

            return null;
        }


        public static string GetIdText(List<int> idList)
        {
            var idText = "0";

            foreach (var id in idList)
            {
                idText += "," + id;
            }

            return idText;
        }
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
